#ifndef INSCRIPCION_H
#define INSCRIPCION_H
#include <iostream>
#include "Fecha.h"
#include "Socio.h"
using namespace std;

class Inscripcion {
private:
	Fecha fecha;
	Socio* socio;
public:
	Inscripcion();
	~Inscripcion();
	Inscripcion(Fecha, Socio*);
	void setFecha(Fecha);
	void setSocio(Socio*);
	Fecha getFecha();
	Socio* getSocio();
};



#endif