#ifndef DTSOCIO_H
#define DTSOCIO_H
#include <iostream>
using namespace std;

class DtSocio {
private: string ci;
	string nombre;
public:
	DtSocio();
	~DtSocio();
	DtSocio(string, string);
	string getCi();
	string getNombre();
};

#endif