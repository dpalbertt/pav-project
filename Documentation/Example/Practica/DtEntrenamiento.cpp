#include "DtEntrenamiento.h"

DtEntrenamiento::DtEntrenamiento(){};
DtEntrenamiento::~DtEntrenamiento(){};
DtEntrenamiento::DtEntrenamiento(int id, string nombre, Turno turno, bool rambla):DtClase(id, nombre, turno){
	this->enRambla = rambla;
}

bool DtEntrenamiento::getEnRambla(){
	return this->enRambla;
}