#include "DtSpinning.h"

DtSpinning::DtSpinning(){};
DtSpinning::~DtSpinning(){};
DtSpinning::DtSpinning(int id, string nombre, Turno turno, int cantB):DtClase(id, nombre, turno){
	this->cantBicicletas = cantB;
};

int DtSpinning::getCantBicicletas(){
	return this->cantBicicletas;
}