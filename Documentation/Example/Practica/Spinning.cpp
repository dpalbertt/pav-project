#include "Spinning.h"

Spinning::Spinning(){};
Spinning::~Spinning(){};
Spinning::Spinning(int id, string nombre, Turno turno, int cantB):Clase(id, nombre, turno){
	this->cantBicicletas = cantB;
};

int Spinning::getCantBicicletas(){
	return this->cantBicicletas;
}

void Spinning::setCantBicicletas(int cantB){
	this->cantBicicletas = cantB;
}