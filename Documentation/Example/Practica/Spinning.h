#ifndef SPINNING_H
#define SPINNING_H
#include "Clase.h"
#include <iostream>
#include "Turno.h"
using namespace std;

class Spinning : public Clase {
private:
	int cantBicicletas;
public:
	Spinning();
	~Spinning();
	Spinning(int, string, Turno, int);
	int getCantBicicletas();
	void setCantBicicletas(int);
	int cupo(){};
};

#endif