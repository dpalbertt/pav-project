#ifndef DTCLASE_H
#define DTCLASE_H
#include "Turno.h"
#include <iostream>
using namespace std;

class DtClase {
private:
	int id;
	string nombre;
	Turno turno;
public:
	DtClase();
	~DtClase();
	DtClase(int, string, Turno);
	int getId();
	string getNombre();
	Turno getTurno();
};

#endif