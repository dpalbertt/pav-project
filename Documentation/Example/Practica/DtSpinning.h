#ifndef DTSPINNING_H
#define DTSPINNING_H
#include "DtClase.h"
#include "Turno.h"
#include <iostream>
using namespace std;

class DtSpinning : public DtClase {
private:
	int cantBicicletas;
public:
	DtSpinning();
	~DtSpinning();
	DtSpinning(int, string, Turno, int);
	int getCantBicicletas();
};

#endif