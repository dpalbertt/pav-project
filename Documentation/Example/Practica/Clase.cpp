#include "Clase.h"

Clase::Clase(){};
Clase::~Clase(){};

Clase::Clase(int id, string nombre, Turno turno){
	this->id = id;
	this->nombre = nombre;
	this->turno = turno;
	this->tope = 0;
	this->insc[0] = NULL;
}

void Clase::setId(int id){
	this->id = id;
}

void Clase::setNombre(string nombre){
	this->nombre = nombre;
}

void Clase::setTurno(Turno turno){
	this->turno = turno;
}

int Clase::getId(){
	return this->id;
}

string Clase::getNombre(){
	return this->nombre;
}

Turno Clase::getTurno(){
	return this->turno;
}

void Clase::addInsc(Inscripcion* m){
	if (this->tope < 50){
		this->insc[this->tope] = m;
		this->tope++;
	}
}
