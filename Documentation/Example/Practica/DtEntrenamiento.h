#ifndef DTENTRENAMIENTO_H
#define DTENTRENAMIENTO_H
#include "DtClase.h"
#include <iostream>
#include "Turno.h"
using namespace std;

class DtEntrenamiento : public DtClase {
private:
	bool enRambla;
public:
	DtEntrenamiento();
	~DtEntrenamiento();
	DtEntrenamiento(int, string, Turno, bool);
	bool getEnRambla();
};

#endif