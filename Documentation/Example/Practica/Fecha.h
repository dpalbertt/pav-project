#ifndef FECHA_H
#define FECHA_H
#include <iostream>
using namespace std;

class Fecha {
	private: int dia;
	int mes;
	int anio;
	public:
		Fecha();
		~Fecha();
		Fecha (int, int, int);
		void setDia(int);
		void setMes(int);
		void setAnio(int);
		int getDia();
		int getMes();
		int getAnio();
};

#endif