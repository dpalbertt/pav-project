#ifndef CLASE_H
#define CLASE_H
#include "Turno.h"
#include "Inscripcion.h"
#include <iostream>
using namespace std;

class Clase {
private:
	int id;
	string nombre;
	Turno turno;
	Inscripcion* insc[50];
	int tope;
public:
	Clase();
	~Clase();
	Clase(int, string, Turno);
	void setId(int);
	void setNombre(string);
	void setTurno(Turno);
	int getId();
	string getNombre();
	Turno getTurno();
	void addInsc(Inscripcion*);
	virtual int cupo() = 0;
};

#endif