#include "Inscripcion.h"

Inscripcion::Inscripcion(){}
Inscripcion::~Inscripcion(){}
Inscripcion::Inscripcion(Fecha fecha, Socio* socio){
	this->fecha = fecha;
	this->socio = socio;
}

void Inscripcion::setFecha(Fecha fecha){
	this->fecha = fecha;
}

Fecha Inscripcion::getFecha(){
	return this->fecha;
}

void Inscripcion::setSocio(Socio* socio){
	this->socio = socio;
}

Socio* Inscripcion::getSocio(){
	return this->socio;
}