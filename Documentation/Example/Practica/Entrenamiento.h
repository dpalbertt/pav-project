#ifndef ENTRENAMIENTO_H
#define ENTRENAMIENTO_H
#include "Clase.h"
#include <iostream>
#include "Turno.h"
using namespace std;

class Entrenamiento : public Clase {
private:
	bool enRambla;
public:
	Entrenamiento();
	~Entrenamiento();
	Entrenamiento(int, string, Turno, bool);
	void setEnRambla(bool);
	bool getEnRambla();
	int cupo(){};
};

#endif