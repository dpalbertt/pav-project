#ifndef SOCIO_H
#define SOCIO_H

#include <iostream>
using namespace std;

class Socio {
	private: string ci;
	string nombre;
	public:
	Socio();
	~Socio();
	Socio(string, string);
	void setCi(string);
	void setNombre(string);
	string getCi();
	string getNombre();
};

#endif