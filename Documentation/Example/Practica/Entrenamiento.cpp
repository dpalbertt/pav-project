#include "Entrenamiento.h"

Entrenamiento::Entrenamiento(){};
Entrenamiento::~Entrenamiento(){};
Entrenamiento::Entrenamiento(int id, string nombre, Turno turno, bool rambla):Clase(id, nombre, turno){
	this->enRambla = rambla;
}

bool Entrenamiento::getEnRambla(){
	return this->enRambla;
}

void Entrenamiento::setEnRambla(bool rambla){
	this->enRambla = rambla;
}