#include "B.h"

B::B(){}

B::~B(){}

B::B(int b,C* c,A* a){
	this->b = b;
	this->myC = c;
	this->myA = a;
}

int B::getB(){
	return this->b;
}

void B::setB(int b){
	this->b = b;
}

A* B::getA(){
	return this->myA;
}

void B::setA(A* a){
	this->myA = a;
}

C* B::getC(){
	return this->myC;
}

void B::setC(C* c){
	this->myC = c;
}

void B::imprimirB(){
	cout<<"Se creo un objeto clase b";
}