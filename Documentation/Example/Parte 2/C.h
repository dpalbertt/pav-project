#ifndef C_H
#define C_H
#include "A.h"
#include "C.h"

//Forward declaration
class B;//Declaracion avanzada de la clase B
class A;//Declaracion avanzada de la clase A

class C {
	private:
		int c;
		A* myA;
		B* myB;
	public:
		C();
		~C();
		C(int,A*,B*);
		int getC();
		void setC(int);
		A* getA();
		void setA(A* a);
		B* getB();
		void setB(B* b);
		void imprimirC();
};

#endif