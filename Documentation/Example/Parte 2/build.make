principal: A.o B.o C.o main.o
	g++ A.o B.o C.o main.o -o test -Wall -g


A.o: A.cpp
	g++ -c A.cpp -g

B.o: B.cpp
	g++ -c B.cpp -g

C.o: C.cpp
	g++ -c C.cpp -g 

main.o: main.cpp
	g++ -c main.cpp -g

clean:
	rm *.o test