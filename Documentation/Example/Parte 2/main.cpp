#include "A.h"
#include "B.h"
#include "C.h"
#include <iostream>
#include <cstdio>

using namespace std;

class A;
class B;
class C;

int main(){
	A a;
	B b;
	C c;

	int aInt=1;
	int bInt=2;
	int cInt=3;

	a.setA(aInt);
	a.imprimirA();
	cout<<" el valor de a es : "<<a.getA()<<endl;;

	b.setB(bInt);
	b.imprimirB();
	cout<<" el valor de b es : "<<b.getB()<<endl;
	c.setC(cInt);
	c.imprimirC();
	cout<<" el valor de c es : "<<c.getC()<<endl;
}