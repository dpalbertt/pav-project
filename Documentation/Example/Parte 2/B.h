#ifndef B_H
#define B_H
#include "B.h"
#include "A.h"

//Forward declaration
class A;//Declaracion avanzada de la clase A
class C;//Declaracion avanzada de la clase C

class B {
	private:
		int b;
		C* myC;
		A* myA;
	public:
		B();
		~B();
		B(int,C*,A*);
		int getB();
		void setB(int);
		C* getC();
		void setC(C* c);
		A* getA();
		void setA(A* a);
		void imprimirB();
};

#endif