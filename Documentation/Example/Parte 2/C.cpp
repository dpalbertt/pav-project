#include "C.h"
#include <string.h>
#include <iostream>

using namespace std;

C::C(){}

C::~C(){}

C::C(int c,A* a,B* b){
	this->c = c;
	this->myA = a;
	this->myB = b;
}

int C::getC(){
	return this->c;
}

void C::setC(int c){
	this->c = c;
}

A* C::getA(){
	return this->myA;
}

void C::setA(A* a){
	this->myA = a;
}

B* C::getB(){
	return this->myB;
}

void C::setB(B* b){
	this->myB = b;
}

void C::imprimirC(){
	cout<<"Se creo un objeto clase c";
}