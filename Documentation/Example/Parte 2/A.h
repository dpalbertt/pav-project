#ifndef A_H
#define A_H
#include "B.h"
#include "C.h"
#include <string.h>
#include <iostream>

using namespace std;

//Forward declaration
class B;//Declaracion avanzada de la clase B
class C;//Declaracion avanzada de la clase C

class A {
	private:
		int a;
		B* myB;
		C* myC;
	public:
		A();
		~A();
		A(int,B*,C*);
		int getA();
		void setA(int);
		B* getB();
		void setB(B* b);
		C* getC();
		void setC(C* c);
		void imprimirA();
};

#endif