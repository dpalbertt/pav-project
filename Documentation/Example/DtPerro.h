#ifndef DTPERRO_H
#define DTPERRO_H
#include "DtMascota.h"
#include "RazaPerro.h"
#include <iostream>
using namespace std;

class DtPerro : public DtMascota {
private:
	RazaPerro raza;
	bool vacunaCachorro;
public:
	DtPerro();
	~DtPerro();
	DtPerro(string, Genero, float, float, RazaPerro, bool);
	RazaPerro getRaza();
	bool getVacuna();

	friend ostream& operator << (ostream&, const DtPerro&);
};

#endif