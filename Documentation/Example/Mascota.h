#ifndef MASCOTA_H
#define MASCOTA_H
#include "Genero.h"
#include <iostream>
using namespace std;

class Mascota {
private:
	string nombre;
	Genero genero;
	float peso;	
public:
	Mascota();
	virtual ~Mascota();
	Mascota(string, Genero, float);
	string getNombre();
	Genero getGenero();
	float getPeso();
	virtual float obtenerRacionDiaria() = 0;
	void setNombre(string);
	void setGenero(Genero);
	void setPeso(float);

};

#endif