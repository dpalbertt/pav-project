#include "DtPerro.h"

DtPerro::DtPerro(){}
DtPerro::~DtPerro(){}
DtPerro::DtPerro(string nombre, Genero genero, float peso, float racion, RazaPerro raza, bool vacuna):DtMascota(nombre, genero, peso, racion){
	this->raza = raza;
	this->vacunaCachorro = vacuna;
}

RazaPerro DtPerro::getRaza(){
	return this->raza;
}

bool DtPerro::getVacuna(){
	return this->vacunaCachorro;
}

ostream& operator << (ostream& salida, const DtPerro& dtc){
	cout << (DtMascota)dtc;
	RazaPerro a = dtc.raza;
	string r;
    switch(a){
    case 1:{
        r="Labrador";
        break;
        }
    case 2:{
        r="Ovejero";
        break;
        }
    case 3:{
        r="Bulldog";
        break;
        }
    case 4:{
        r="Pitbull";
        break;
        }
    case 5:{
        r="Collie";
        break;
        }
    case 6:{
        r="Pekines";
        break;
        }
    case 7:{
        r="Otro";
        break;
            }
        }
	cout << "Raza: " << r << endl;
	if (dtc.vacunaCachorro)
		cout << "Vacunas: Si" << endl;
	else 
		cout << "Vacunas: No" << endl;
	return salida;
}
