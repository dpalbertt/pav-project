#ifndef DTCONSULTA_H
#define DTCONSULTA_H
#include "DtFecha.h"
#include <iostream>
using namespace std;

class DtConsulta {
private:
	DtFecha* fechaConsulta;
	string motivo;
public:
	DtConsulta();
	~DtConsulta();
	DtConsulta(DtFecha*, string);
	DtFecha* getFecha();
	string getMotivo();
};

#endif