#include "DtGato.h"

DtGato::DtGato(){}
DtGato::~DtGato(){}
DtGato::DtGato(string nombre, Genero genero, float peso, float racion, TipoPelo pelo):DtMascota(nombre, genero, peso, racion){
	this->tipoPelo = pelo;
}

TipoPelo DtGato::getTipoPelo(){
	return this->tipoPelo;
}



ostream& operator << (ostream& salida, const DtGato& dtc){
	cout << (DtMascota)dtc;
	string r;
	TipoPelo a = dtc.tipoPelo;
    switch(a){
    case 1:{
        r="Corto";
        break;
    }
    case 2:{
        r="Mediano";
        break;
    }
    case 3:{
        r="Largo";
        break;
        }
    }
	cout << "Tipo de pelo: " << r << endl;
	return salida;
}