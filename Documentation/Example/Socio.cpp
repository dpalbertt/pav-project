#include "Socio.h"

Socio::Socio(){
	this->topeMasc = 0;
	this->topeConsul = 0;
}
Socio::~Socio(){}
Socio::Socio(string ci, string nombre, DtFecha* fecha, Mascota* mascota){
	this->ci = ci;
	this->nombre = nombre;
	this->fechaIngreso = fecha;
	this->masc[0] = mascota;
	this->topeMasc = 1;
	this->topeConsul = 0;
}

string Socio::getCi(){
	return this->ci;
}

string Socio::getNombre(){
	return this->nombre;
}

DtFecha* Socio::getFechaIngreso(){
	return this->fechaIngreso;
}

int Socio::getTopeMascotas() {
	return this->topeMasc;
}
int Socio::getTopeConsultas() {
	return this->topeConsul;
}

void Socio::setCi(string ci){
	this->ci = ci;
}

void Socio::setNombre(string nombre){
	this->nombre = nombre;
}

void Socio::setFechaIngreso(DtFecha* fecha){
	this->fechaIngreso = fecha;
}

void Socio::addMascota(Mascota* mascota){
	this->masc[this->topeMasc] = mascota;
	this->topeMasc++;
}

void Socio::addConsulta(Consulta* consulta){
	this->consul[this->topeConsul] = consulta;
	this->topeConsul++;
}

Consulta* Socio::getConsulta(int i){
	return this->consul[i];
}

Mascota* Socio::getMascotas(int i){
	return this->masc[i];
}