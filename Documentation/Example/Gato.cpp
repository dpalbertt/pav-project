#include "Gato.h"

Gato::Gato(){}
Gato::~Gato(){}
Gato::Gato(string nombre, Genero genero, float peso,float racion ,TipoPelo pelo):Mascota(nombre, genero, peso){
	this->tipoPelo = pelo;
}

TipoPelo Gato::getTipoPelo(){
	return this->tipoPelo;
}

void Gato::setTipoPelo(TipoPelo pelo){
	this->tipoPelo = pelo;
}

void Gato::mostrarGato(Gato* gato){
	cout<<"El nombre del gato es: "<<gato->getNombre()<<endl;
	int b= gato->getGenero();
	switch(b){
		case 0:{
			cout<<"El genero del gato es : Macho"<<endl;	
			break;	
		}
		case 1:{
			cout<<"El genero del gato es : Hembra"<<endl;
			break;
		}
	}
	int c= gato->getTipoPelo();
	switch(c){
		case 1:{
			cout<<"El tipo de pelo del gato es corto\n";
			break;
		}
		case 2:{
			cout<<"El tipo de pelo del gato es mediano\n";
			break;
		}
		case 3:{
			cout<<"El tipo de pelo del gato es largo\n";
			break;
		}
	}
	
	cout<<"La racion del gato es :"<<gato->obtenerRacionDiaria()<<endl;
}