#include "DtConsulta.h"

DtConsulta::DtConsulta(){}
DtConsulta::~DtConsulta(){}
DtConsulta::DtConsulta(DtFecha* fecha, string motivo){
	this->fechaConsulta = fecha;
	this->motivo = motivo;
}

DtFecha* DtConsulta::getFecha(){
	return this->fechaConsulta;
}

string DtConsulta::getMotivo(){
	return this->motivo;
}