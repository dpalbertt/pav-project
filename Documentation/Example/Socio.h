#ifndef SOCIO_H
#define SOCIO_H
#include "Mascota.h"
#include "Consulta.h"
#include "DtFecha.h"
#define MAX_MASCOTAS 10
#define MAX_CONSULTAS 20

class Socio {
private:
	string ci;
	string nombre;
	DtFecha* fechaIngreso;
	Mascota* masc[MAX_MASCOTAS];
	int topeMasc;
	Consulta* consul[MAX_CONSULTAS];
	int topeConsul;
public:
	Socio();
	~Socio();
	Socio(string, string, DtFecha*, Mascota*);
	string getCi();
	string getNombre();
	DtFecha* getFechaIngreso();
	Mascota* getMascotas(int);
	Consulta* getConsulta(int);
	int getTopeMascotas();
	int getTopeConsultas();
	void setCi(string);
	void setNombre(string);
	void setFechaIngreso(DtFecha*);
	void addMascota(Mascota*);
	void addConsulta(Consulta*);
};

#endif