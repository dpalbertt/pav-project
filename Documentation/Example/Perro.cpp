#include "Perro.h"

Perro::Perro(){}
Perro::~Perro(){}
Perro::Perro(string nombre, Genero genero, float peso,float racion, RazaPerro raza, bool vacuna):Mascota(nombre, genero, peso){
	this->raza = raza;
	this->vacunaCachorro = vacuna;
}


RazaPerro Perro::getRaza(){
	return this->raza;
}

bool Perro::getVacuna(){
	return this->vacunaCachorro;
}

void Perro::setRaza(RazaPerro raza){
	this->raza = raza;
}

void Perro::setVacuna(bool vacuna){
	this->vacunaCachorro = vacuna;
}

void Perro::mostrarPerro(Perro* perro){
	cout<<"El nombre del perro es: "<<perro->getNombre()<<endl;
	int a= perro->getVacuna();
	if(a==1){
	cout<<"El perro esta vacunado\n";
	}else{
		cout<<"El perro no esta vacunado\n";
	}
	int b= perro->getGenero();
	switch(b){
		case 0:{
			cout<<"El genero del perro es : Macho"<<endl;	
			break;	
		}
		case 1:{
			cout<<"El genero del perro es : Hembra"<<endl;
			break;
		}
	}
	int c= perro->getRaza();
	switch(c){
		case 1:{
			cout<<"La raza del perro es Labrador\n";
			break;
		}
		case 2:{
			cout<<"La raza del perro es Ovejero\n";
			break;
		}
		case 3:{
			cout<<"La raza del perro es Bulldog\n";
			break;
		}
		case 4:{
			cout<<"La raza del perro es Pitbull\n";
			break;
		}
		case 5:{
			cout<<"La raza del perro es Collie\n";
			break;
		}
		case 6:{
			cout<<"La raza del perro es Pekines\n";
			break;
		}
		case 7:{
			cout<<"La raza del perro esta en la categoria Otros\n";
			break;
		}

	}
	
	cout<<"La racion del perro es :"<<perro->obtenerRacionDiaria()<<endl;
}