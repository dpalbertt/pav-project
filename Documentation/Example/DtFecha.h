#ifndef DTFECHA_H
#define DTFECHA_H

class DtFecha {
private:
	int dia;
	int mes;
	int anio;
public:
	DtFecha();
	~DtFecha();
	DtFecha(int, int, int);
	int getDia();
	int getMes();
	int getAnio();
	void setDia(int);
	void setMes(int);
	void setAnio(int);
	friend bool operator <(const DtFecha &,const DtFecha &);
};

#endif