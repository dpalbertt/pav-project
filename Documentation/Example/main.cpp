#include "DtFecha.h"
#include "DtConsulta.h"
#include "Consulta.h"
#include "Socio.h"
#include "DtGato.h"
#include "DtMascota.h"
#include "DtPerro.h"
#include "Gato.h"
#include "Genero.h"
#include "Mascota.h"
#include "Perro.h"
#include "RazaPerro.h"
#include "TipoPelo.h"
#define MAX_CONSULTAS 20
#define MAX_SOCIOS 200
#include <string.h>
#include <iostream>
#include <stdexcept>
#include <typeinfo>
#include <stdlib.h>


using namespace std;

struct socios{
	Socio* socios[MAX_SOCIOS];
	int tope;
}coleccionSocios;

Socio* buscarSocio(string ci){
	int i=0;
	bool encontreSocio=false;
	while (!encontreSocio && (i < coleccionSocios.tope)) {
		if (coleccionSocios.socios[i]->getCi() == ci)
			encontreSocio = true;
		else
			i++;
	}
	if(encontreSocio){
		return coleccionSocios.socios[i];
	}else{
		return NULL;
	}
}

void existeSocio(string ci){
	int i=0;
	bool encontreSocio=false;
	while (!encontreSocio && (i < coleccionSocios.tope)) {
		if (coleccionSocios.socios[i]->getCi()==ci)
			encontreSocio = true;
		i++;
	}
	if (!encontreSocio)
		throw invalid_argument("ERROR: NO EXISTE EL SOCIO\n");
}

void eliminarConsultas(Socio* socio){
	/*int i=0;
	Consulta* consultaAux= socio->getConsulta(i);
	while(consultaAux!=NULL){
		delete socio->getConsulta(i);
		i++;
		consultaAux=socio->getConsulta(i);
	}*/
}

void registrarSocio(string ci, string nombre, DtMascota& dtMascota){
	if(coleccionSocios.tope!=MAX_SOCIOS){
		Socio* socio = new Socio();
		socio->setCi(ci);
		socio->setNombre(nombre);
		DtFecha* fechita = new DtFecha();
		int valor;
		cout << "Que dia es?" <<endl;
		cin >> valor;
		fechita->setDia(valor);
		cout << "Que mes es?" <<endl;
		cin >> valor;
		fechita->setMes(valor);
		cout << "Que anio es?" <<endl;
		cin >> valor;
		fechita->setAnio(valor);
		socio->setFechaIngreso(fechita);
		try{
			DtPerro& perroAux= dynamic_cast<DtPerro&>(dtMascota);
			Perro* perrito = new Perro(perroAux.getNombre(), perroAux.getGenero(), perroAux.getPeso(), perroAux.getRacion(), perroAux.getRaza(), perroAux.getVacuna());
			socio->addMascota(perrito);
			coleccionSocios.socios[coleccionSocios.tope]=socio;
			coleccionSocios.tope++;
		}catch(std::bad_cast){
			try{
				DtGato& gatoAux= dynamic_cast<DtGato&>(dtMascota);
				Gato* gatito= new Gato(gatoAux.getNombre(), gatoAux.getGenero(), gatoAux.getPeso(), gatoAux.getRacion(), gatoAux.getTipoPelo());
				socio->addMascota(gatito);
				coleccionSocios.socios[coleccionSocios.tope]=socio;
				coleccionSocios.tope++;
			}catch(std::bad_cast){
				cout<<"ERROR: no atendemos ese tipo de mascota, por favor, retirese."<<endl;
			}
		}
	}else{
		cout<<"ERROR: no hay mas cupos para socios."<<endl;
	}
}

void agregarMascota(string ci, DtMascota& dtMascota){
	try{
		existeSocio(ci);
		Socio* socAux=buscarSocio(ci);
   		try{
			DtPerro& perroAux= dynamic_cast<DtPerro&>(dtMascota);
			Perro* perrito = new Perro(perroAux.getNombre(), perroAux.getGenero(), perroAux.getPeso(), perroAux.getRacion(), perroAux.getRaza(), perroAux.getVacuna());
			socAux->addMascota(perrito);
		}catch(std::bad_cast){
			try{
				DtGato& gatoAux= dynamic_cast<DtGato&>(dtMascota);
				Gato* gatito= new Gato(gatoAux.getNombre(), gatoAux.getGenero(), gatoAux.getPeso(), gatoAux.getRacion(), gatoAux.getTipoPelo());
				socAux->addMascota(gatito);
			}catch(std::bad_cast){
				cout<<"ERROR: no atendemos ese tipo de mascota, por favor , retirese."<<endl;
			}
		}
	}catch(invalid_argument& e){
		cout << e.what() << endl;
	}
}

void ingresarConsulta(string motivo, string ci){
	try{
		existeSocio(ci);
		Socio* socAux=buscarSocio(ci);
		DtFecha* fechita = new DtFecha();
		int valor;
		cout << "Que dia es?" <<endl;
		cin >> valor;
		fechita->setDia(valor);
		cout << "Que mes es?" <<endl;
		cin >> valor;
		fechita->setMes(valor);
		cout << "Que anio es?" <<endl;
		cin >> valor;
		fechita->setAnio(valor);
		Consulta* consultita = new Consulta(fechita,motivo);
		socAux->addConsulta(consultita);
	}catch(invalid_argument& e){
		cout << e.what() << endl;
	}
}


DtConsulta** verConsultasAntesDeFecha(DtFecha& fecha, string ciSocio, int& cantConsultas){

	try{
		existeSocio(ciSocio);
		Socio* socAux=buscarSocio(ciSocio);
		if (socAux->getTopeConsultas() < cantConsultas)
			cantConsultas = socAux->getTopeConsultas();
		DtConsulta** result = new DtConsulta*[cantConsultas]; //hacer que pida memoria
		int index = 0;

		for (int i=0; i < socAux->getTopeConsultas(); i++) {
			DtFecha fechita = DtFecha(socAux->getConsulta(i)->getFecha()->getDia(), socAux->getConsulta(i)->getFecha()->getMes(), socAux->getConsulta(i)->getFecha()->getAnio());
			if (fechita < fecha) {
				if (index < cantConsultas) {
					DtConsulta* consultita = new DtConsulta(socAux->getConsulta(i)->getFecha(), socAux->getConsulta(i)->getMotivo());
					result[index] = consultita;
					index++;
				}
			}
			if (index < cantConsultas)
				cantConsultas = index;
		}

		return result;

	}catch(invalid_argument& e){
		throw invalid_argument(e);
	}
}

void eliminarSocio(string ci){
	try{
		existeSocio(ci);
		int i = 0;
		bool salir = false;
		while (i < coleccionSocios.tope) {
			if ((coleccionSocios.socios[i]->getCi() == ci) && !salir)
				salir = true;
			else
				i++;
		}
		Socio* inter = coleccionSocios.socios[coleccionSocios.tope];
		coleccionSocios.socios[i] = inter;
		coleccionSocios.tope--;

	}catch(invalid_argument& e){
		cout << e.what() << endl;
	}
}

DtMascota** obtenerMascotas(string ci, int& cantMascotas) {
	try{
		existeSocio(ci);
		Socio* socAux=buscarSocio(ci);
		if (socAux->getTopeMascotas() < cantMascotas)
			cantMascotas = socAux->getTopeMascotas();
		DtMascota** mascot = new DtMascota*[cantMascotas];
		int index = 0;
		for (int i=0; i < socAux->getTopeMascotas(); i++) {
			if (index < cantMascotas){
				Mascota* aux = socAux->getMascotas(i);
				Perro* perroAux = dynamic_cast<Perro*>(aux);
				if (perroAux != NULL){
					DtPerro* perrito = new DtPerro(perroAux->getNombre(), perroAux->getGenero(), perroAux->getPeso(), perroAux->getPeso() * 0.025, perroAux->getRaza(), perroAux->getVacuna());
					mascot[index] = perrito;
				}
				Gato* gatoAux = dynamic_cast<Gato*>(aux);
				if(gatoAux != NULL){
					DtGato* gatito= new DtGato(gatoAux->getNombre(), gatoAux->getGenero(), gatoAux->getPeso(), gatoAux->getPeso() * 0.21, gatoAux->getTipoPelo());
					mascot[index] = gatito;
				}
				
				index++;
			}
		}
		return mascot;
	}
	catch(std::bad_cast){
        //cout<<"ERROR: no atendemos ese tipo de mascota, por favor , retirese."<<endl;
    }

	/*catch(invalid_argument& e){
		cout << e.what() << endl;
	}*/
}

void menu(){
	cout<<"Bienvenido elija la opcion.\n\n1) Registrar socio\n2) Agregar mascota\n3) Ingresar consulta\n4) Ver consultas antes de fecha\n5) Eliminar socio\n6) Ver Mascota\n0) Salir"<<endl<<endl;
}


RazaPerro switchRaza(int a){
	RazaPerro r;
	bool salir = false;
	while(!salir){
        switch(a){
            case 1:{
                r=Labrador;
                salir = true;
                break;
            }
            case 2:{
                r=Ovejero;
                salir = true;
                break;
            }
            case 3:{
                r=Bulldog;
                salir = true;
                break;
            }
            case 4:{
                r=Pitbull;
                salir = true;
                break;
            }
            case 5:{
                r=Collie;
                salir = true;
                break;
            }
            case 6:{
                r=Pekines;
                salir = true;
                break;
            }
            case 7:{
                r=Otro;
                salir = true;
                break;
            }
            default:
                cout<<"\nSeleccione un numero entre 1 y 7.\n\n";
                cin>>a;
        }
	}
	return r;
}

TipoPelo switchPelo(int a){
	TipoPelo r;
	bool salir = false;
	while(!salir){
        switch(a){
            case 1:{
                r=Corto;
                salir = true;
                break;
            }
            case 2:{
                r=Mediano;
                salir = true;
                break;
            }
            case 3:{
                r=Largo;
                salir = true;
                break;
            }
            default:
                cout<<"\nSeleccione opcion valida.\n";
                cin>>a;
        }
	}
	return r;
}

Genero switchGenero(int a){
	Genero r;
	bool salir = false;
	while(!salir){
        switch(a){
            case 1:{
                r=Macho;
                salir = true;
                break;
            }
            case 2:{
                r=Hembra;
                salir = true;
                break;
            }
            default:
                cout<<"\nIngrese 1-para Macho o 2-para Hembra."<<endl;
                cin>>a;
        }
	}
    return r;
}

bool switchVacuna(int a){
	bool v;
	bool salir = false;
	while(!salir){
        switch(a){
            case 1:{
                v=true;
                salir = true;
                break;
            }
            case 0:{
                v=false;
                salir = true;
                break;
            }
            default:
                cout<<"\nIngrese 1-para Si o 0-para No."<<endl;
                cin>>a;
        }
	}
    return v;
}


int main(){
	int a;
	string ci;
	string nombre;
	string nomMascota;
	int generoInt;
	Genero genero;
	float peso;
	bool vacuna;
	int tipo;
	TipoPelo pelo;
	bool estado = false;
	coleccionSocios.tope = 0;
	bool exit=false;
	DtMascota** mascotas;
	DtConsulta** consultas;
	do{
        bool salir = false;
		menu();
		bool estado = false;
		cin>>a;
		switch(a){
			case 1:{//REGISTRAR A UN SOCIO SE SOLICITA DATOS DE PERSONA Y MASCOTA
				cout<<endl<<"Ingrese su cedula:";
				cin>>ci;
				Socio* socAux=buscarSocio(ci);
				if(socAux==NULL){
					cout<<"Ingrese su nombre:";
					cin>>nombre;
					cout<<"Ingrese el nombre de su mascota:";
					cin>>nomMascota;
					cout<<"Ingrese el peso de su mascota:";
					cin>>peso;
					cout<<"Que genero es su mascota?\n1)Macho\n2)Hembra\n";
					cin>>generoInt;
					genero = switchGenero(generoInt);
					cout<<"Que mascota desea registrar?"<<endl<<"1)Perro"<<endl<<"2)Gato"<<endl;
					DtPerro perro;
					DtGato gato;
                    while(!salir){
                        cin>>tipo;
                        switch (tipo){
                            case 1: {
                                cout<<"Raza de su perro\n1)Labrador\n2)Ovejero\n3)Bulldog\n4)Pitbull\n5)Collie\n6)Pekines\n7)Otro"<<endl;
                                int raza;
                                cin>>raza;
                                RazaPerro r=switchRaza(raza);
                                cout<< "Su perro esta vacunado?"<<endl<<"1)Si"<<endl<<"2)No"<<endl;
                                int vacunaInt;
                                cin>> vacunaInt;
                                vacuna = switchVacuna(vacunaInt);
                                perro = DtPerro(nomMascota, genero, peso, 0, r, vacuna);
                                registrarSocio(ci, nombre, perro);
                                salir = true;
                                break;
                            }
                            case 2: {
                                cout << "Que tipo de pelo tiene su gato? "<<endl<<"1)Corto"<<endl<<"2)Mediano"<<endl<<"3)Largo"<<endl;
                                int peloInt;
                                cin>> peloInt;
                                TipoPelo pelo = switchPelo(peloInt);
                                gato = DtGato(nomMascota, genero, peso, 0, pelo);
                                registrarSocio(ci, nombre, gato);
                                salir = true;
                                break;
                            }
                            default:
                                cout<<"Ingrese 1-para Perro o 2-para Gato."<<endl;
                        }
					}

				}
				else {
					cout<<"ERROR: El socio ya existe"<<endl;
				}
				cout << endl;
				break;
			}//FIN DEL CASE 1 DEL SWITCH DEL MENU
			case 2:{
				//agregar mascota
				cout<<endl<<"Ingrese su cedula:";
				cin>>ci;
				Socio* socAux=buscarSocio(ci);
				if(socAux !=NULL){
                    cout<<"Ingrese el nombre de su mascota:";
                    cin>>nomMascota;
                    cout<<"Ingrese el peso de su mascota:";
                    cin>>peso;
                    cout<<"Que genero es su mascota?\n1)Macho\n2)Hembra\n";
                    cin>>generoInt;
                    genero = switchGenero(generoInt);
                    cout<<"Que mascota desea registrar?"<<endl<<"1)Perro"<<endl<<"2)Gato"<<endl;
                    DtPerro perro;
                    DtGato gato;
                    while(!salir){
                        cin>>tipo;
                        switch (tipo){
                            case 1: {
                                cout<<"Raza de su perro\n1)Labrador\n2)Ovejero\n3)Bulldog\n4)Pitbull\n5)Collie\n6)Pekines\n7)Otro"<<endl;
                                int raza;cin>>raza;
                                RazaPerro r=switchRaza(raza);
                                cout << "Su perro esta vacunado?"<<endl<<"1)Si"<<endl<<"2)No"<<endl;
                                int vacunaInt;
                                cin>> vacunaInt;
                                vacuna = switchVacuna(vacunaInt);
                                perro = DtPerro(nomMascota, genero, peso, 0, r, vacuna);
                                agregarMascota(ci, perro);
                                salir = true;
                                break;
                            }
                            case 2: {
                                cout << "Que tipo de pelo tiene su gato? "<<endl<<"1)Corto"<<endl<<"2)Mediano"<<endl<<"3)Largo"<<endl;
                                int peloInt;
                                cin>> peloInt;
                                TipoPelo pelo = switchPelo(peloInt);
                                gato = DtGato(nomMascota, genero, peso, 0, pelo);
                                agregarMascota(ci, gato);
                                salir = true;
                                break;
                            }
                            default:
                                cout<<"Ingrese 1-para Perro o 2-para Gato."<<endl;
                        }
                    }
                    cout << endl;
				}
				else{
                    cout<<"Cedula invalida\n";
				}
				break;
			}//FIN DEL SWITCH DE AGREGAR MASCOTA
			case 3:{
				// ingresar consulta
				cout<< "Ingrese su cedula"<<endl;
				cin>>ci;
				cout<< "Ingrese su motivo"<<endl;
				string motivo;
				cin>>motivo;
				ingresarConsulta(motivo, ci);
				cout<< endl;
                break;
			}//FIN DEL SWITCH DE INGRESAR CONSULTA
			case 4:{
				//Ver consultas antes de fecha
				DtFecha fechita = DtFecha();
				int valor;
				cout<< "Ingresamos fecha para ver anteriores a esta" << endl;
				cout<< "Que dia es?" <<endl;
				cin>> valor;
				fechita.setDia(valor);
				cout<< "Que mes es?" <<endl;
				cin>> valor;
				fechita.setMes(valor);
				cout<< "Que anio es?" <<endl;
				cin>> valor;
				fechita.setAnio(valor);
				cout<< "Ingrese cedula del socio: ";
				string ciSocio;
				cin>> ciSocio;
				cout<< "Ingrese cantidad de consultas: ";
				int cantConsultas; cin >> cantConsultas;
				try{
					consultas = verConsultasAntesDeFecha(fechita, ciSocio, cantConsultas);
					for (int i = 0; i < cantConsultas; i++){
						cout << "Consulta" << i+1 << ": " << endl << "Motivo: " << consultas[i]->getMotivo() << endl;
					}
				}catch(invalid_argument& e){
					cout<<e.what()<<endl;
				}
                break;
			}//FIN DEL SWITCH DE INGRESAR CONSULTA
			case 5:{
				//Eliminar socio
				cout << "Ingrese su cedula" <<endl;
				cin>>ci;
				eliminarSocio(ci);
				break;
			}//FIN DEL SWITCH DE ELIMINAR SOCIO
			case 6:{
				//Ver Mascota
				cout<< "Ingrese cedula del socio: ";
				//string ciSocio;
				cin>> ci;
				cout<< "Ingrese cantidad de mascotas: ";
				int cantMascotas;
				cin>> cantMascotas;
                //mascotas = obtenerMascotas(ci, cantMascotas);
                try{
                    mascotas = obtenerMascotas(ci, cantMascotas);
                    for (int i = 0; i < cantMascotas; i++){
                        DtPerro* aux = dynamic_cast<DtPerro*>(mascotas[i]);
						if (aux != NULL)
							cout<< *aux <<endl;
						DtGato* aux1 = dynamic_cast<DtGato*>(mascotas[i]);
						if (aux1 != NULL)
							cout << *aux1 << endl;
					}

                }
                catch(invalid_argument& e){
                    cout << e.what() << endl;
                }
				break;
			}
			case 0:{
				exit = true;
				break;
			}//FIN DEL SWITCH DE INGRESAR CONSULTA
			default:{
				cout<<"ERROR:Ingrese una opcion valida"<<endl<<endl;
				break;
			}//SI SE INGRESA UN NUMERO O STRING QUE NO CORRESPONDE A NINGUNO SE IMPRIME ERROR Y VUELVE AL MENU

		}
	} while (!exit);
}
