#ifndef CONSULTA_H
#define CONSULTA_H
#include "DtFecha.h"
#include <iostream>
using namespace std;

class Consulta {
private:
	DtFecha* fechaConsulta;
	string motivo;
public:
	Consulta();
	~Consulta();
	Consulta(DtFecha*, string);
	DtFecha* getFecha();
	string getMotivo();
	void setFecha(DtFecha*);
	void setMotivo(string);
};

#endif