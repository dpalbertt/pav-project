#ifndef GATO_H
#define GATO_H
#include "Mascota.h"
#include "TipoPelo.h"

class Gato : public Mascota {
private:
	TipoPelo tipoPelo;
public:
	Gato();
	~Gato();
	Gato(string, Genero, float,float, TipoPelo);
	TipoPelo getTipoPelo();
	float obtenerRacionDiaria() { return this->getPeso() * 0.015; };
	void setTipoPelo(TipoPelo);
	void mostrarGato(Gato*);
};

#endif