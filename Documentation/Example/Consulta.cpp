#include "Consulta.h"

Consulta::Consulta(){}
Consulta::~Consulta(){}
Consulta::Consulta(DtFecha* fecha, string motivo){
	this->fechaConsulta = fecha;
	this->motivo = motivo;
}

DtFecha* Consulta::getFecha(){
	return this->fechaConsulta;
}

string Consulta::getMotivo(){
	return this->motivo;
}

void Consulta::setFecha(DtFecha* fecha){
	this->fechaConsulta = fecha;
}

void Consulta::setMotivo(string motivo){
	this->motivo = motivo;
}

