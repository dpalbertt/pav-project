#ifndef PERRO_H
#define PERRO_H
#include "Mascota.h"
#include "RazaPerro.h"

class Perro : public Mascota {
private:
	RazaPerro raza;
	bool vacunaCachorro;
public:
	Perro();
	~Perro();
	Perro(string, Genero, float,float, RazaPerro, bool);
	RazaPerro getRaza();
	bool getVacuna();
	float obtenerRacionDiaria() { return this->getPeso() * 0.025; };
	void setRaza(RazaPerro);
	void setVacuna(bool);
	void mostrarPerro(Perro* perro);
};

#endif