#ifndef DTMASCOTA_H
#define DTMASCOTA_H
#include <iostream>
#include "Genero.h"
using namespace std;

class DtPerro;

class DtMascota {
private:
	string nombre;
	Genero genero;
	float peso;
	float racionDiaria;
	
public:
	DtMascota();
	virtual ~DtMascota();
	DtMascota(string, Genero, float, float);
	string getNombre();
	Genero getGenero();
	float getPeso();
	float getRacion();
	friend ostream& operator << (ostream&, const DtMascota&);
};

#endif