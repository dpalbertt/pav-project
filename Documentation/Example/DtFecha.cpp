#include "DtFecha.h"
#include <iostream>
using namespace std;

DtFecha::DtFecha(){}
DtFecha::~DtFecha(){}
DtFecha::DtFecha(int dia, int mes, int anio){
	this->dia = dia;
	this->mes = mes;
	this->anio = anio;
}

int DtFecha::getDia(){
	return this->dia;
}

int DtFecha::getMes(){
	return this->mes;
}

int DtFecha::getAnio(){
	return this->anio;
}

void DtFecha::setDia(int dia){
	this->dia = dia;
}
void DtFecha::setMes(int mes){
	this->mes = mes;
}
void DtFecha::setAnio(int anio){
	this->anio = anio;
}

bool operator <(const DtFecha &p1,const DtFecha &p2){
	if (p1.anio < p2.anio){
		return true;
	}
	else if (p1.anio == p2.anio){

		if (p1.mes < p2.mes){
			return true;
		}
		else if (p1.mes == p2.mes){
			if (p1.dia < p2.dia){
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	else
		 return false;
}