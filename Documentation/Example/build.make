principal: DtMascota.o DtPerro.o DtGato.o DtConsulta.o DtFecha.o Mascota.o Perro.o Gato.o Consulta.o Socio.o main.o
	g++ DtMascota.o DtPerro.o DtGato.o DtConsulta.o DtFecha.o Mascota.o Perro.o Gato.o Consulta.o Socio.o main.o -o test -Wall -g
DtMascota.o: DtMascota.cpp
	g++ -c DtMascota.cpp -g
DtPerro.o: DtPerro.cpp
	g++ -c DtPerro.cpp -g
DtGato.o: DtGato.cpp
	g++ -c DtGato.cpp -g
DtConsulta.o: DtConsulta.cpp
	g++ -c DtConsulta.cpp -g
DtFecha.o: DtFecha.cpp
	g++ -c DtFecha.cpp -g
Mascota.o: Mascota.cpp
	g++ -c Mascota.cpp -g
Perro.o: Perro.cpp
	g++ -c Perro.cpp -g
Gato.o: Gato.cpp
	g++ -c Gato.cpp -g
Consulta.o: Consulta.cpp
	g++ -c Consulta.cpp -g
Socio.o: Socio.cpp
	g++ -c Socio.cpp -g
main.o: main.cpp
	g++ -c main.cpp -g
clean: 
	rm *.o test
