#include "DtMascota.h"

DtMascota::DtMascota(){}
DtMascota::~DtMascota(){}
DtMascota::DtMascota(string nombre, Genero genero, float peso, float racion){
	this->nombre = nombre;
	this->genero = genero;
	this->peso = peso;
	this->racionDiaria = racion;
}

string DtMascota::getNombre(){
	return this->nombre;
}

Genero DtMascota::getGenero(){
	return this->genero;
}

float DtMascota::getPeso(){
	return this->peso;
}

float DtMascota::getRacion(){
	return this->racionDiaria;
}

ostream& operator << (ostream& salida, const DtMascota& dtc){
	cout << "\nNombre: " << dtc.nombre << "\nGenero: ";
	 if (dtc.genero == 0)
	 	cout << "Macho";
	else
		cout << "Hembra";
	cout << "\nPeso: " << dtc.peso << " Kilogramos\nRacion Diaria:"<< dtc.racionDiaria<<" Kilogramos\n";

	return salida;
}