#ifndef DTGATO_H
#define DTGATO_H
#include <iostream>
using namespace std;
#include "DtMascota.h"
#include "TipoPelo.h"

class DtGato : public DtMascota {
private:
	TipoPelo tipoPelo;
public:
	DtGato();
	~DtGato();
	DtGato(string, Genero, float, float, TipoPelo);
	TipoPelo getTipoPelo();

	friend ostream& operator << (ostream&, const DtGato&);
};

#endif