#include "DtSala.h"

DtSala::DtSala(){}
DtSala::~DtSala(){}
DtSala::DtSala(int id, int capacidad){
	this->id = id;
	this->capacidad = capacidad;
}

int DtSala::getId(){
	return this->id;
}

int DtSala::getCapacidad(){
	return this->capacidad;
}

bool operator <(const DtSala &p1,const DtSala &p2){
	if (p1.id < p2.id)
		return true;
	else
		return false;
}

ostream& operator <<(ostream& salida, const DtSala& dtS ){
	cout<< "Id: "<< dtS.id <<endl;
	cout<< "Capacidad: "<< dtS.capacidad;
	return salida;
}