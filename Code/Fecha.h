#ifndef FECHA_H
#define FECHA_H
#include <iostream>
using namespace std;

class Fecha {
private:
	int dia;
	int mes;
	int anio;
public:
	Fecha();
	~Fecha();
	Fecha(int,int,int);
	int getDia();
	int getMes();
	int getAnio();
	void setDia(int);
	void setMes(int);
	void setAnio(int);

	friend ostream& operator <<(ostream&, Fecha&);
};

#endif