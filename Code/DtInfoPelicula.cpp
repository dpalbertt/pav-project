#include "DtInfoPelicula.h"

DtInfoPelicula::DtInfoPelicula(){}
DtInfoPelicula::~DtInfoPelicula(){}
DtInfoPelicula::DtInfoPelicula(string poster, string sinopsis){
	this->poster = poster;
	this->sinopsis = sinopsis;
}

string DtInfoPelicula::getPoster(){
	return this->poster;
}

string DtInfoPelicula::getSinopsis(){
	return this->sinopsis;
}

ostream& operator <<(ostream& salida, DtInfoPelicula& info){
	cout<<"Poster: "<<info.poster<<endl;
	cout<<"Sinopsis: "<<info.sinopsis<<endl;
	return salida;
}