#include "ControlCine.h"
#include "Cine.h"
#include "ManejadorCine.h"
#include <iostream>
#include "ControlPelicula.h"

using namespace std;

ControlCine::ControlCine(){}
ControlCine::~ControlCine(){}

ControlCine* ControlCine::instancia = NULL;

ControlCine* ControlCine::getInstancia(){
	if (instancia == NULL)
		instancia = new ControlCine();
	return instancia;
}

set<DtFuncion> ControlCine::selectCine(int id){ 
	set<DtFuncion> funciones;
	ManejadorCine* mC = ManejadorCine::getInstancia();
	try {
		Cine* cine = mC->buscarCine(id);
		if (cine->obtenerSalas().empty())
			throw invalid_argument("Este cine no tiene salas");
		else {
			for (auto sala : cine->obtenerSalas()){
				if (sala.second->obtenerFunciones().empty()){
					throw invalid_argument("Este cine no presenta funciones");
				}
				else {
					for (auto funcion : sala.second->obtenerFunciones()){
						ControlPelicula* contP = ControlPelicula::getInstanciaPelicula();
						if (funcion.second->getPelicula()->getTitulo() == contP->getIdPelicula()){ //la funcion tiene que corresponder a la pelicula seleccionada
							DtFuncion aux = DtFuncion(funcion.second->getDia(), funcion.second->getId(), funcion.second->getHorario());
							funciones.insert(aux);
						}
					}
				}
				return funciones; //arreglar esto, es para que compile
			}
		}
	} catch (invalid_argument e){
		throw invalid_argument(e.what());
	}
}

set<DtSala> ControlCine::seleccionarCine(int id){
	this->cineId = id;
	ManejadorCine* mC = ManejadorCine::getInstancia();
	try {
		Cine* cine = mC->buscarCine(id);
		set<DtSala> salas;
		if (cine->obtenerSalas().empty())
			throw invalid_argument("Este cine no tiene salas");
		else {
			for (auto sala : cine->obtenerSalas()){
				DtSala dtS = DtSala(sala.second->getId(), sala.second->getCapacidad());
				salas.insert(dtS);
			}
			return salas;
		}
	} catch (invalid_argument e){
		throw invalid_argument(e.what()); //le paso el error que tira buscarCine
	}
}

void ControlCine::altaCine(Direccion direccion){
	ManejadorCine* mC= ManejadorCine::getInstancia();
	Cine cine = Cine(Cine::getIdAuto(),direccion);
	mC->agregarCine(cine);
	this->cineId = cine.getId();

}

void ControlCine::agregarSalas(set<int> capacidadSalas){
	ManejadorCine* mC = ManejadorCine::getInstancia();
	map<int, Sala*> salas;
	for(auto capacidad : capacidadSalas){
		Sala* sala = new Sala(Sala::getIdAuto(),capacidad);
		salas.insert(pair<int, Sala*>(sala->getId(), sala));
	}
	Cine* cine = mC->buscarCine(this->cineId);
	cine->setSalas(salas);
}

void ControlCine::ingresarDireccion(Direccion){
	this->direccion = direccion;
}

int ControlCine::getCineId(){
	return this->cineId;
}