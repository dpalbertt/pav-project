#ifndef DTCINE_H
#define DTCINE_H
#include "Direccion.h"
#include <iostream>
using namespace std;

class DtCine {
	private:
		int id;
		Direccion direccion;
	public:
		DtCine();
		~DtCine();
		DtCine(int,Direccion);
		int getId();
		Direccion getDireccion();
		friend bool operator <(const DtCine &,const DtCine &);
		friend ostream& operator << (ostream&, const DtCine&);
};

#endif
