#include "DtCine.h"
#include "Direccion.h"
#include <iostream>
using namespace std;

DtCine::DtCine(){}
DtCine::~DtCine(){}
DtCine::DtCine(int id,Direccion direccion){
	this->id = id;
	this->direccion = direccion;
}

int DtCine::getId(){
	return this->id;
}

Direccion DtCine::getDireccion(){
	return this->direccion;
}

bool operator <(const DtCine &p1,const DtCine &p2){
	if (p1.id < p2.id)
		return true;
	else
		return false;
}

ostream& operator <<(ostream& salida, const DtCine& dtC){
	cout<<dtC.direccion;
	cout<< "Id: " << dtC.id<<endl;
	return salida;
}