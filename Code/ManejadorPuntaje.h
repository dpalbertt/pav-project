#ifndef MANEJADORPUNTAJE_H
#define MANEJADORPUNTAJE_H
#include "Puntaje.h"
#include <set>
#include <iostream>

using namespace std;

class ManejadorPuntaje {
	private:
		ManejadorPuntaje();
		static ManejadorPuntaje* instancia;
		multiset<Puntaje> puntajes;
	public:
		static ManejadorPuntaje* getManejadorPuntaje();
		float obtenerPuntajePelicula(string pelicula);
		multiset<Puntaje> getPuntajes();
		~ManejadorPuntaje();
		void ingresarPuntaje(Puntaje);
		void quitarPuntaje(int);
};

#endif