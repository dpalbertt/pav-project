#include "Base.h"

Base* Base::instancia = NULL;

Base::Base(){}

Base::~Base(){}

Base* Base::getBase(){
	if(instancia ==NULL){
		instancia = new Base();
	}
	return instancia;
}

void Base::crearDatos(){
	//USUARIOS
	Usuario user1 =  Usuario("Leandro","123456","https://s03.s3c.es/imag/_v0/580x400/8/f/0/CR7-celebra-Balon-gritazo-2015-EFE.jpg");
	Usuario user2 =  Usuario("Albert","123456","https://sevilla.abc.es/gurme/wp-content/uploads/2010/03/pollo-asado-citricos-1920-1200x675.jpg");
	Usuario user3 =  Usuario("Yona","thanos","https://i.kym-cdn.com/entries/icons/original/000/029/442/cover_inevitable.jpg");
	user1.setAdmin(true);
	user2.setAdmin(true);
	user3.setAdmin(false);
	ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
	mU->agregarUsuario(user1);
	mU->agregarUsuario(user2);
	mU->agregarUsuario(user3);

	Usuario* user1Aux= mU->buscarUsuario(user1.getNickName());
	Usuario* user2Aux= mU->buscarUsuario(user2.getNickName());
	Usuario* user3Aux= mU->buscarUsuario(user3.getNickName());

	//DIRECCIONES
	Direccion dir1= Direccion("Igua",2525);
	Direccion dir2= Direccion("Av. Italia",2025);
	Direccion dir3= Direccion("Benito Blanco",2125);
	/*Direccion dir4= Direccion("Av.Bolivia",2225);
	Direccion dir5= Direccion("Mataojo",2325);
	Direccion dir6= Direccion("18 de julio",2425);
	Direccion dir7= Direccion("Cno.Carrasco",2825);
	Direccion dir8= Direccion("Uruguay",2645); */


	//FECHAS 	
	Fecha fecha1= Fecha(27,6,2019);
	Fecha fecha2= Fecha(28,5,2019);
	Fecha fecha3= Fecha(29,4,2019);
	Fecha fecha4= Fecha(30,3,2019);
	/*Fecha fecha5= Fecha(20,2,2019);
	Fecha fecha6= Fecha(21,1,2019);
	Fecha fecha7= Fecha(1,1,2019);*/

	//HORARIOS

	Horario hor1= Horario("15:30","18:30");
	Horario hor2= Horario("16:30","19:30");
	Horario hor3= Horario("17:30","20:30");
	Horario hor4= Horario("14:30","16:45");

	//CINES
	Cine cine1 =  Cine(Cine::getIdAuto(),dir1);
	Cine cine2 =  Cine(Cine::getIdAuto(),dir2);
	Cine cine3 =  Cine(Cine::getIdAuto(),dir3);
	/*Cine cine4 =  Cine(4,dir4);
	Cine cine5 =  Cine(5,dir5);
	Cine cine6 =  Cine(6,dir6);
	Cine cine7 =  Cine(7,dir7);
	Cine cine8 =  Cine(8,dir8); */

	ManejadorCine* mC= ManejadorCine::getInstancia();
	mC->agregarCine(cine1);
	mC->agregarCine(cine2);
	mC->agregarCine(cine3);
	/* mC->agregarCine(cine4);
	mC->agregarCine(cine5);
	mC->agregarCine(cine6);
	mC->agregarCine(cine7);
	mC->agregarCine(cine8); */


	//PELICULAS
	Pelicula pel1= Pelicula("StarWars","Guerra de naves","\n──────────────▐█████───────\n──────▄▄████████████▄──────\n────▄██▀▀────▐███▐████▄────\n──▄██▀───────███▌▐██─▀██▄──\n─▐██────────▐███─▐██───██▌─\n─██▌────────███▌─▐██───▐██─\n▐██────────▐███──▐██────██▌\n██▌────────███▌──▐██────▐██\n██▌───────▐███───▐██────▐██\n██▌───────███▌──▄─▀█────▐██\n██▌──────▐████████▄─────▐██\n██▌──────█████████▀─────▐██\n▐██─────▐██▌────▀─▄█────██▌\n─██▌────███─────▄███───▐██─\n─▐██▄──▐██▌───────────▄██▌─\n──▀███─███─────────▄▄███▀──\n──────▐██▌─▀█████████▀▀────\n──────███──────────────────\n\n");
	Pelicula pel2= Pelicula("StarWars2","Guerra de naves","unposter2");
	Pelicula pel3= Pelicula("StarWars3","Guerra de naves","unposter3");
	Pelicula pel4= Pelicula("Avengers","Marvel","unposter4"); //no usar esta

	Cine* ptrCine1 = mC->buscarCine(cine1.getId());
	Cine* ptrCine2 = mC->buscarCine(cine2.getId());
	Cine* ptrCine3 = mC->buscarCine(cine3.getId());

	// seteando cines a las peliculas

	/*pel1.addCine(ptrCine1);
	pel2.addCine(ptrCine1);
	pel2.addCine(ptrCine3);
	pel3.addCine(ptrCine2);
	pel3.addCine(ptrCine3);
	pel4.addCine(ptrCine2);*/

	ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
	mP->agregarPelicula(pel1);
	mP->agregarPelicula(pel2);
	mP->agregarPelicula(pel3);
	mP->agregarPelicula(pel4);

	Pelicula* ptrPeli1 = mP->buscarPelicula(pel1.getTitulo());
	Pelicula* ptrPeli2 = mP->buscarPelicula(pel2.getTitulo());
	Pelicula* ptrPeli3 = mP->buscarPelicula(pel3.getTitulo());
	Pelicula* ptrPeli4 = mP->buscarPelicula(pel4.getTitulo());

	ptrPeli1->addCine(ptrCine1);
	ptrPeli1->addIdCines(ptrCine1->getId());
	ptrPeli2->addCine(ptrCine1);
	ptrPeli2->addIdCines(ptrCine2->getId());
	ptrPeli2->addCine(ptrCine3);
	ptrPeli2->addIdCines(ptrCine3->getId());
	ptrPeli3->addCine(ptrCine2);
	ptrPeli3->addIdCines(ptrCine2->getId());
	ptrPeli3->addCine(ptrCine3);
	ptrPeli3->addIdCines(ptrCine3->getId());
	ptrPeli4->addCine(ptrCine2);
	ptrPeli4->addIdCines(ptrCine2->getId());



	//FUNCIONES 

	Funcion fun1= Funcion(fecha1,Funcion::getIdAuto(),hor1);
	Funcion fun2= Funcion(fecha2,Funcion::getIdAuto(),hor2);
	Funcion fun3= Funcion(fecha3,Funcion::getIdAuto(),hor3);
	Funcion fun4= Funcion(fecha4,Funcion::getIdAuto(),hor4);

	fun1.setPelicula(ptrPeli1);
	fun2.setPelicula(ptrPeli2);
	fun3.setPelicula(ptrPeli3);
	fun4.setPelicula(ptrPeli4);

	ManejadorFuncion* mF = ManejadorFuncion::getManejadorFuncion();
	mF->agregarFuncion(fun1);
	mF->agregarFuncion(fun2);
	mF->agregarFuncion(fun3);
	mF->agregarFuncion(fun4);

	Funcion* func1Aux = mF->buscarFuncion(fun1.getId());
	Funcion* func2Aux = mF->buscarFuncion(fun2.getId());
	Funcion* func3Aux = mF->buscarFuncion(fun3.getId());
	Funcion* func4Aux = mF->buscarFuncion(fun4.getId());


		//SALAS
	Sala* sala1 = new Sala(Sala::getIdAuto(), 30);
	Sala* sala2 = new Sala(Sala::getIdAuto(), 25);
	Sala* sala3 = new Sala(Sala::getIdAuto(), 30);
	Sala* sala4 = new Sala(Sala::getIdAuto(), 31);
	Sala* sala5 = new Sala(Sala::getIdAuto(), 32);
	Sala* sala6 = new Sala(Sala::getIdAuto(), 33);
	Sala* sala7 = new Sala(Sala::getIdAuto(), 34);
	Sala* sala8 = new Sala(Sala::getIdAuto(), 35);

	Funcion* fun1Aux = mF->buscarFuncion(fun1.getId());
	Funcion* fun2Aux = mF->buscarFuncion(fun2.getId());
	Funcion* fun3Aux = mF->buscarFuncion(fun3.getId());
	Funcion* fun4Aux = mF->buscarFuncion(fun4.getId());

	sala1->addFuncion(fun1Aux);
	sala1->addFuncion(fun2Aux);
	sala2->addFuncion(fun3Aux);
	sala2->addFuncion(fun4Aux);
	sala3->addFuncion(fun2Aux);
	sala3->addFuncion(fun3Aux);

	// Seteando salas a los Cines

	ptrCine1->addSala(sala1);
	ptrCine2->addSala(sala2);
	ptrCine3->addSala(sala3);

	/*map<int, Sala*> salas1;
	map<int, Sala*> salas2;
	map<int, Sala*> salas3;
	salas1.insert(pair<int, Sala*>(sala1->getId(), sala1));
	salas2.insert(pair<int, Sala*>(sala2->getId(), sala2));
	salas3.insert(pair<int, Sala*>(sala3->getId(), sala3));

	ptrCine1->setSalas(salas1);
	ptrCine2->setSalas(salas2);
	ptrCine3->setSalas(salas1);*/


	//COMENTARIOS
	Comentario com1 = Comentario();Comentario com2 =Comentario();Comentario com3 =Comentario();
	Comentario resp1 = Comentario();Comentario resp2 = Comentario();Comentario resp3 = Comentario();Comentario resp4= Comentario();
	Comentario respR1 = Comentario();Comentario respR2 = Comentario();Comentario respR3= Comentario();
	com1.setTexto("Una cagada la pelicula");
	com1.setPelicula(ptrPeli1);
	com1.setNickName(user1.getNickName());
	com1.setIdAuto();

	com2.setTexto("Agregar Comentario Principal de Albert");
	com2.setPelicula(ptrPeli2);
	com2.setNickName(user2.getNickName());
	com2.setIdAuto();

	com3.setTexto("Agregar Comentario  Principal de Yona");
	com3.setPelicula(ptrPeli3);
	com3.setNickName(user3.getNickName());
	com3.setIdAuto();

	resp1.setTexto("Respuesta Comentario Principal de Albert");
	resp1.setPelicula(ptrPeli2);
	resp1.setNickName(user1.getNickName());
	resp1.setIdAuto();

	resp2.setTexto("Respuesta Comentario De leandro");
	resp2.setPelicula(ptrPeli1);
	resp2.setNickName(user2.getNickName());
	resp2.setIdAuto();

	resp3.setTexto("Respuesta Comentario Principal de Albert");
	resp3.setPelicula(ptrPeli2);
	resp3.setNickName(user3.getNickName());
	resp3.setIdAuto();

	resp4.setTexto("Respuesta Comentario Principal de YOna");
	resp4.setPelicula(ptrPeli3);
	resp4.setNickName(user2.getNickName());
	resp4.setIdAuto();

	com2.addRespuesta(resp1);
	com1.addRespuesta(resp2);
	com2.addRespuesta(resp3);
	com3.addRespuesta(resp4);


	respR1.setTexto("Respuesta a Respuesta Comentario 2");
	respR1.setPelicula(ptrPeli2);
	respR1.setNickName(user2.getNickName());
	respR1.setIdAuto();

	respR2.setTexto("Respuesta a Respuesta Comentario 1");
	respR2.setPelicula(ptrPeli1);
	respR2.setNickName(user1.getNickName());
	respR2.setIdAuto();

	respR3.setTexto("Respuesta a Respuesta Comentario 1");
	respR3.setPelicula(ptrPeli1);
	respR3.setNickName(user3.getNickName());
	respR3.setIdAuto();
	
	resp1.addRespuesta(respR1);
	resp2.addRespuesta(respR2);
	resp2.addRespuesta(respR3);

	user1Aux->addComentario(com1);
	user2Aux->addComentario(com2);
	user3Aux->addComentario(com3);

	//RESERVAS

	Reserva res1= Reserva((float)453.90,2,fun1Aux,user1Aux);
	Reserva res2= Reserva(500,1,fun2Aux,user2Aux);
	Reserva res3= Reserva(300,3,fun3Aux,user3Aux);

	//PUNTAJES 

	Puntaje punt1= Puntaje(1,ptrPeli1,user1Aux);
	Puntaje punt2= Puntaje(2,ptrPeli1,user2Aux);
	Puntaje punt3= Puntaje(3,ptrPeli1,user3Aux);
	Puntaje punt4= Puntaje(4,ptrPeli2,user1Aux);
	Puntaje punt5= Puntaje(5,ptrPeli2,user2Aux);
	Puntaje punt6= Puntaje(6,ptrPeli2,user3Aux);
	Puntaje punt7= Puntaje(7,ptrPeli2,user1Aux);
	Puntaje punt8= Puntaje(8,ptrPeli3,user2Aux);
	Puntaje punt9= Puntaje(9,ptrPeli3,user3Aux);
	Puntaje punt10= Puntaje(5,ptrPeli4,user1Aux);
	Puntaje punt11= Puntaje(6,ptrPeli4,user2Aux);
	Puntaje punt12= Puntaje(7,ptrPeli4,user3Aux);
	Puntaje punt13= Puntaje(9,ptrPeli4,user2Aux);

	ManejadorPuntaje* mPun = ManejadorPuntaje::getManejadorPuntaje();
	mPun->ingresarPuntaje(punt1);
	mPun->ingresarPuntaje(punt2);
	mPun->ingresarPuntaje(punt3);
	mPun->ingresarPuntaje(punt4);
	mPun->ingresarPuntaje(punt5);
	mPun->ingresarPuntaje(punt6);
	mPun->ingresarPuntaje(punt7);
	mPun->ingresarPuntaje(punt8);
	mPun->ingresarPuntaje(punt9);
	mPun->ingresarPuntaje(punt10);
	mPun->ingresarPuntaje(punt11);
	mPun->ingresarPuntaje(punt12);
	mPun->ingresarPuntaje(punt13);



	//TARJETA CREDITO 
	Credito credito1= Credito();
	credito1.setPorcentajeDescuento("25");
	credito1.setFinanciera("OCA");

	Credito credito2= Credito();
	credito2.setPorcentajeDescuento("30");
	credito2.setFinanciera("Scotiabank");

	Credito credito3= Credito();
	credito3.setPorcentajeDescuento("40");
	credito3.setFinanciera("BBVA");

	//TARJETA DEBITO 
	Debito debito1= Debito();
	debito1.setBanco("BBVA");

	Debito debito2= Debito();
	debito2.setBanco("Scotiabank");

	Debito debito3= Debito();
	debito3.setBanco("OCA");

}

