#ifndef DTPELICULACOMPLETA_H
#define DTPELICULACOMPLETA_H

#include <iostream>
using namespace std;

class DtPeliculaCompleta{
private:
	string titulo;
	float puntaje;
public:
	DtPeliculaCompleta();
	~DtPeliculaCompleta();
	DtPeliculaCompleta(string,float);
	string getTitulo();
	float getPuntaje();
	void setTitulo(string);
	void setPuntaje(float);
};

#endif