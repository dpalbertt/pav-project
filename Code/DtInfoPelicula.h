#ifndef DTINFOPELICULA_H
#define DTINFOPELICULA_H
#include <iostream>
using namespace std;

class DtInfoPelicula{
private:
	string poster;
	string sinopsis;
public:
	DtInfoPelicula();
	~DtInfoPelicula();
	DtInfoPelicula(string,string);
	string getPoster();
	string getSinopsis();
	void setPoster();
	void setSinopsis();

	friend ostream& operator << (ostream&, DtInfoPelicula&);
};

#endif