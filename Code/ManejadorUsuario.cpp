#include "ManejadorUsuario.h"
#include "ManejadorPelicula.h"
//#include "ManejadorCine.h"
//#include "Usuario.h"
#include "Comentario.h"
#include <iostream>
#include "Sesion.h"

using namespace std;

ManejadorUsuario* ManejadorUsuario::instancia = NULL;

int inc = 0;

ManejadorUsuario::ManejadorUsuario(){}
ManejadorUsuario::~ManejadorUsuario(){}



ManejadorUsuario* ManejadorUsuario::getInstancia(){

	if(instancia == NULL ){
		instancia = new ManejadorUsuario();
	}
	return instancia;
}

map<string, Usuario*> ManejadorUsuario::getUsuarios(){
	return this->usuarios;	
}

Usuario* ManejadorUsuario::buscarUsuario(string usuario){
	Usuario* user;
	bool found = false;
	map<string, Usuario*>::iterator iter = this->usuarios.begin();
	while (iter != this->usuarios.end() && !found){
		if (iter->first == usuario){
			user = iter->second;
			found = true;
		}
		iter++;
	} 
	if (!found)
		throw invalid_argument("No se encontró el usuario");
	return user;
}

void ManejadorUsuario::agregarComentario(Usuario* user, Comentario comentario){
	user->addComentario(comentario);
}

void ManejadorUsuario::agregarUsuario(Usuario user){
	Usuario* aux = new Usuario(user.getNickName(), user.getContrasenia(), user.getUrlFoto());
	aux->setAdmin(user.getAdmin());
	aux->setComentarios(user.getComentarios());
	this->usuarios.insert(pair<string, Usuario*>(user.getNickName(), aux));
}

void ManejadorUsuario::imprimirComentarios(set<Comentario> comentarios,string titulo){
	ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
	Pelicula* pel = mP->buscarPelicula(titulo);
	for(auto aux: comentarios){
		if(aux.getPelicula()->getTitulo() == titulo){	
			for(int i = 0 ; i!= inc; i++){
				cout<<"\t";
			}
			inc ++;
		cout<<aux.getNick()<<"("<<aux.getId()<<"): "<<aux.getTexto()<<"\n";
		imprimirComentarios(aux.getRespuestas(),titulo);
		inc = 0;
		}
	}
}

void ManejadorUsuario::mostrarComentarios(string titulo){
	cout<<endl;
	for(auto aux : this->usuarios/*multiset de usuarios*/){
		imprimirComentarios(aux.second->getComentarios(),titulo);
		cout<<endl;
		}
	}

void ManejadorUsuario::responderComentario(int id, string texto,string titulo){
	cout<<endl;
	Sesion* sesion = Sesion::getInstancia();
	string nick = sesion->getNickName();
	ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
	Pelicula* pel = mP->buscarPelicula(titulo);
	Comentario respuesta = Comentario();
	respuesta.setIdAuto();
	respuesta.setTexto(texto);
	respuesta.setPelicula(pel);
	respuesta.setNickName(nick);
	//agregaria setearle un set de comentarios vacio
	for(auto aux : this->usuarios){
		//estoy dentro de un usuario
		int contador = 0;
		bool found = false;
		Comentario insertar;
		for( auto aux2 : aux.second->getComentarios()){
			//comentarios de ese usuario
			if(aux2.getId() == id){
				found = true;
				insertar = aux2;
				//creo comentario a insertar
				set<Comentario> clavo = aux2.getRespuestas();
				clavo.insert(respuesta);
				insertar.setRespuestas(clavo);
				//cree el comentario denuevo
			} 
			else {
				++contador;
			}

		}
		//termine de iterar los comentarios
		if (found){
			aux.second->quitarComentario(contador); //aqui quito el comentario
			aux.second->addComentario(insertar); //agrego el comentario
			//puede ser que esto no ande para responder a respuestas, habria que hacerlo recursivo para eso
			//no tengo tiempo para hacerlo recursivo
		}
		else
			throw invalid_argument("No existe el comentario a responder");

	}
}


