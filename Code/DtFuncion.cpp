#include "DtFuncion.h"

DtFuncion::DtFuncion(){}
DtFuncion::~DtFuncion(){}
DtFuncion::DtFuncion(Fecha dia, int id, Horario horario){
	this->dia = dia;
	this->id = id;
	this->horario = horario;
}

Fecha DtFuncion::getDia(){
	return this->dia;
}

int DtFuncion::getId(){
	return this->id;
}

Horario DtFuncion::getHorario(){
	return this->horario;
}

ostream& operator <<(ostream& salida, DtFuncion& dtF){
	cout<<dtF.dia<<endl;
	cout<<"ID: "<<dtF.id<<endl;
	cout<<dtF.horario<<endl;
	return salida;
}

bool operator <(const DtFuncion &p1,const DtFuncion &p2){
	if (p1.id < p2.id)
		return true;
	else
		return false;
}