#include "ControlSala.h"
#include "Sala.h"
#include "DtSala.h"
#include "ManejadorCine.h"
#include "ControlCine.h"

ControlSala::ControlSala(){}
ControlSala::~ControlSala(){}

ControlSala* ControlSala::instancia = NULL;

ControlSala* ControlSala::getInstancia(){
	if(instancia == NULL){
		instancia = new ControlSala();
	}
	return instancia;
}

void ControlSala::ingresarCapacidad(int capacidad){
	this->capacidad = capacidad;
}

void ControlSala::seleccionarSala(int idSala){
	ControlCine* contCine = ControlCine::getInstancia();
	if (contCine->getCineId() == idSala)
		this->sala = idSala;
	else
		throw invalid_argument("No existe la sala seleccionada");
}	

int ControlSala::getIdSala(){
	return this->sala;
}