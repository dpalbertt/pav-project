#include "DtComentario.h"
#include <iostream>
#include <string.h>

using namespace std;

DtComentario::DtComentario(){}
DtComentario::~DtComentario(){}
DtComentario::DtComentario(string nickName, string texto, set<DtComentario> respuestas){
	this->nickName = nickName;
	this->texto = texto;
	this->respuestas = respuestas;
}

DtComentario::DtComentario(Comentario com){
	this->nickName = com.getNick();
	this->texto = com.getTexto();
	set<DtComentario> aux;
	set<Comentario>::iterator iter = com.getRespuestas().begin();
	for (iter; iter != com.getRespuestas().end(); ++iter){
		Comentario comentario = *iter;
		DtComentario amigo = DtComentario(comentario);
		aux.insert(amigo);
	}
	this->respuestas = aux;
}

string DtComentario::getNickName(){
	return this->nickName;
}

string DtComentario::getTexto(){
	return this->texto;
}

set<DtComentario> DtComentario::getRespuestas(){
	return this->respuestas;
}

ostream& operator <<(ostream& salida, DtComentario& dtF){
	cout << "Nick: " << dtF.nickName <<endl;
	cout << "Comentario: "<< dtF.texto <<endl;
	for (set<DtComentario>::iterator iter = dtF.respuestas.begin(); iter != dtF.respuestas.end(); ++iter){
		DtComentario com = *iter;
		cout << "\t" << com << endl;
	}
	return salida;
}

bool operator <(const DtComentario &p1, const DtComentario &p2){
	if (p1.texto < p2.texto)
		return true;
	else
		return false;
}

void DtComentario::setTexto(string texto){
	this->texto = texto;
}