#include "Credito.h"

Credito::Credito(){}
Credito::~Credito(){}
Credito::Credito(float costo, int asientos, Funcion* funcion, Usuario* usuario, string financiera, string descuento):Reserva(costo, asientos, funcion, usuario){
	this->financiera = financiera;
	this->porcentajeDescuento = descuento;
}

void Credito::setPorcentajeDescuento(string descuento){
	this->porcentajeDescuento = descuento;
}

void Credito::setFinanciera(string financiera){
	this->financiera = financiera;
}

string Credito::getPorcentajeDescuento(){
	return this->porcentajeDescuento;
}

string Credito::getFinanciera(){
	return this->financiera;
}