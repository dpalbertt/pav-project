#ifndef MANEJADORCINE_H
#define MANEJADORCINE_H
#include "Cine.h"
#include <map>
#include <list>
#include <string>
#include <iostream>

using namespace std;

class ManejadorCine {
private:
	static ManejadorCine* instancia;
	map<int, Cine*> cines;
	ManejadorCine();
public:
	static ManejadorCine* getInstancia();
	map<int, Cine*> getCines();
	void agregarCine(Cine);
	Cine* buscarCine(int);
	virtual ~ManejadorCine();
};

#endif