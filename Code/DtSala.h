#ifndef DTSALA_H
#define DTSALA_H

#include <iostream>
using namespace std;

class DtSala{
private:
	int id;
	int capacidad;
public:
	DtSala();
	~DtSala();
	DtSala(int,int);
	int getId();
	int getCapacidad();
	void setId(int);
	void setCapacidad(int);
	friend bool operator <(const DtSala &,const DtSala &);
	friend ostream& operator << (ostream&,const DtSala&);
};

#endif