#ifndef DEBITO_H
#define DEBITO_H
#include "Reserva.h"

class Debito : public Reserva {
private:
	string banco;
public:
	Debito();
	~Debito();
	Debito(float, int, Funcion*, Usuario*, string);
	void setBanco(string);
	string getBanco();
};

#endif