#ifndef DTPELICULA_H
#define DTPELICULA_H
#include <iostream>
using namespace std;

class DtPelicula{
private:
	string titulo;
	string sinopsis;
	string poster;
	float puntajePromedio;
public:
	DtPelicula();
	~DtPelicula();
	DtPelicula(string,string,string);
	string getTitulo();
	string getSinopsis();
	string getPoster();
	float getPuntajePromedio();
	friend bool operator <(const DtPelicula &,const DtPelicula &);
};

#endif
