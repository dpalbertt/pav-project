#include "DtPeliculaCompleta.h"

DtPeliculaCompleta::DtPeliculaCompleta(){}
DtPeliculaCompleta::~DtPeliculaCompleta(){}
DtPeliculaCompleta::DtPeliculaCompleta(string titulo, float puntaje){
	this->titulo = titulo;
	this->puntaje = puntaje;
}

string DtPeliculaCompleta::getTitulo(){
	return this->titulo;
}

float DtPeliculaCompleta::getPuntaje(){
	return this->puntaje;
}
