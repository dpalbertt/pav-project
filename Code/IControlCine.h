#ifndef ICONTROLCINE_H
#define ICONTROLCINE_H
#include "DtSala.h"
#include "Direccion.h"
#include "DtFuncion.h"
#include <iostream>
#include <set>
#include "Cine.h"
using namespace std;

class IControlCine {
public:
	virtual set<DtFuncion> selectCine(int) = 0;
	virtual set<DtSala> seleccionarCine(int) = 0;
	virtual void altaCine(Direccion) = 0;
	virtual void agregarSalas(set<int>) = 0;
	virtual void ingresarDireccion(Direccion) = 0;
};

#endif
