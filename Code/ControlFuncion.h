#ifndef CONTROLFUNCION_H
#define CONTROLFUNCION_H

#include <iostream>
#include "Horario.h"
#include "Fecha.h"
#include "IControlFuncion.h"
#include "DtFuncion.h"
#include "ManejadorFuncion.h"

using namespace std;

class ControlFuncion : public IControlFuncion {
public:
    void seleccionarFuncion(int);
    void altaFuncion(Horario, Fecha);
    static ControlFuncion* getInstancia();
    void ingresarHorario();
    Horario getHorario();
    Fecha getFecha();
    int getId();
    void setId(int);

private:
    static ControlFuncion* control;
    ControlFuncion();
    ~ControlFuncion();
    //atributos
    int id;
    Horario horario;
    Fecha fecha;
};

#endif
