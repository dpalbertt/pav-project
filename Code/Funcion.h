#ifndef FUNCION_H
#define FUNCION_H

#include "Fecha.h"
#include "Horario.h"
#include "Pelicula.h"

class Funcion {
private:
    static int autoId;
    Fecha dia;
    int id;
    Horario horario;
    Pelicula* pelicula;
public:
    Funcion();
    ~Funcion();
    Funcion(Fecha, int, Horario);
    static int getIdAuto();
    void setDia(Fecha);
    void setId(int);
    void setHorario(Horario);
    void setPelicula(Pelicula*);
    Fecha getDia();
    int getId();
    Horario getHorario();
    Pelicula* getPelicula();
    friend bool operator <(const Funcion &,const Funcion &);
};

#endif