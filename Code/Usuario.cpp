#include "Usuario.h"


Usuario::Usuario(){}
Usuario::~Usuario(){}
Usuario::Usuario(string nickName, string contrasenia, string urlFoto){
	this->nickName = nickName;
	this->contrasenia = contrasenia;
	this->urlFoto = urlFoto;
}

string Usuario::getNickName(){
	return this->nickName;
}

void Usuario::setNickName(string nickName){
	this->nickName = nickName;
}

string Usuario::getContrasenia(){
	return this->contrasenia;
}

void Usuario::setContrasenia(string contrasenia){
	this->contrasenia = contrasenia;
}

string Usuario::getUrlFoto(){
	return this->urlFoto;
}

void Usuario::setUrlFoto(string urlFoto){
	this->urlFoto = urlFoto;
}

bool Usuario::getAdmin(){
	return this->admin;
}

void Usuario::setAdmin(bool admin){
	this->admin = admin;
}

set<Comentario> Usuario::getComentarios(){
	return this->comentarios;
}

void Usuario::setComentarios(set<Comentario> comentarios){
	this->comentarios = comentarios;
}

void Usuario::addComentario(Comentario comentario){
	this->comentarios.insert(comentario);
}

void Usuario::quitarComentario(int aux){
	set<Comentario>::iterator iter = this->comentarios.begin();
	for (int i = 0; i < aux; i++){
		++iter;
	}
	this->comentarios.erase(iter);
}

bool operator <(const Usuario &u1, const Usuario &u2){
	if(u1.id<u2.id)
		return true;
	else 
		return false;

}

