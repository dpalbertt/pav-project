#ifndef CINE_H
#define CINE_H
#include <iostream>
#include "Sala.h"
#include "Direccion.h"

using namespace std;

class Cine{
	private:
		static int idAuto;
		int id;
		Direccion direccion; //cambio aca, se quito el * para que no sea puntero. | albert 16/6
		map<int, Sala*> salas; //este si es set, pertenece a cada cine
	public:
		Cine();
		~Cine();
		Cine(int, Direccion);
		int getId();
		Direccion getDireccion();
		static int getIdAuto();
		void setId(int);
		void setDireccion(Direccion);
		void setSalas(map<int, Sala*>);
		map<int, Sala*> obtenerSalas();
		void addSala(Sala*);
		friend bool operator <(const Cine &,const Cine &);
};

#endif