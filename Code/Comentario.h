#ifndef COMENTARIO_H
#define COMENTARIO_H
#include "Pelicula.h"

using namespace std;

class Comentario{
	private:
		static int idAuto;
		string nick;
		string texto;
		Pelicula* pelicula;
		set<Comentario> respuestas;
		bool esRespuesta;
		int id;
	public:
		Comentario();
		~Comentario();
		Comentario(string,string, Pelicula*);
		string getTexto();
		void setTexto(string);
		void setPelicula(Pelicula*);
		void addRespuesta(Comentario);
		void setNickName(string);
		set<Comentario> getRespuestas();
		Pelicula* getPelicula();
		string getNick();
		bool getEsRespuesta();
		void setEsRespuesta(bool);
		void setIdAuto();
		int getId();
		void setRespuestas(set<Comentario>);
		friend bool operator <(const Comentario &,const Comentario &);
};

#endif
