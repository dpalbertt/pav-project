#ifndef ICONTROLSALA_H
#define ICONTROLSALA_H
#include <iostream>
#include "DtSala.h"
using namespace std;

class IControlSala{
public:
	virtual void ingresarCapacidad(int) = 0;
	virtual void seleccionarSala(int) = 0;
};

#endif