#include "Direccion.h"
#include <iostream>

Direccion::Direccion(){}
Direccion::~Direccion(){}
Direccion::Direccion(string calle, int numero){
	this->calle = calle;
	this->numero = numero;
}

string Direccion::getCalle(){
	return this->calle;
}

int Direccion::getNumero(){
	return this->numero;
}

void Direccion::setCalle(string calle){
 	this->calle = calle;
}

void Direccion::setNumero(int numero){
	this->numero = numero;
}

ostream& operator <<(ostream& salida ,const Direccion& dir){
	string calle = dir.calle;
	int numero = dir.numero;
	cout<<"Direccion: " <<calle <<","<<numero<<"."<<endl;
	return salida;
}