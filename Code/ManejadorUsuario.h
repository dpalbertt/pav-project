#ifndef MANEJADORUSUARIO_H
#define MANEJADORUSUARIO_H
#include "Usuario.h"
//#include "Comentario.h"
#include <string>
//#include <iostream>

using namespace std;

class ManejadorUsuario{
private:
	static ManejadorUsuario* instancia;
	//map<string,Usuario*> usuarios;
	map<string, Usuario*> usuarios;
	ManejadorUsuario();
public:
	static ManejadorUsuario* getInstancia();
	map<string, Usuario*> getUsuarios();
	Usuario* buscarUsuario(string);
	virtual ~ManejadorUsuario();
	void agregarComentario(Usuario*,Comentario);
	void agregarUsuario(Usuario);
	void mostrarComentarios(string);
	void imprimirComentarios(set<Comentario>,string);
	void responderComentario(int,string,string);

};

#endif

