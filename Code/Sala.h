#ifndef SALA_H
#define SALA_H
#include "Funcion.h"
#include "DtSala.h"
#include <map>

using namespace std;

class Sala{
	private:
		static int idAuto;
		int id;
		int capacidad;
		map<int,Funcion*> funciones;

	public:
		Sala();
		~Sala();
		Sala(int, int);
		static int getIdAuto();
		int getId();
		int getCapacidad();
		void setId(int);
		void setCapacidad(int);
		map<int,Funcion*> obtenerFunciones();
		void addFuncion(Funcion*);
		void quitarFuncion(int);
		friend bool operator <(const Sala &,const Sala &);
};
#endif