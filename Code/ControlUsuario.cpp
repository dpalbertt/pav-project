#include "ControlUsuario.h"
#include "ManejadorUsuario.h"
#include <iostream>
#include <stdexcept>
#include <typeinfo>
#include "Sesion.h"
#include <conio.h>
#include "ManejadorUsuario.h"
using namespace std;

ControlUsuario::ControlUsuario(){}
ControlUsuario::~ControlUsuario(){}

ControlUsuario* ControlUsuario::instancia = NULL;

string ControlUsuario::getNickname(){
	return this->nickName;
}

bool ControlUsuario::chequearPassword(string password){
		string nick = this->getNickname();
		ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
		Usuario* user = mU->buscarUsuario(nick); //aca no hace falta try catch, porque ya se verifico en iniciar sesion
		if (user->getContrasenia() == password)
			return true;
		else
			return false;
}

void ControlUsuario::iniciarSesion(string nick){
	char a;
	string password;
	Usuario* user;//#MODIF user pasa a ser global
	ControlUsuario* cU = ControlUsuario::getInstancia();
	ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
	try {
		user = mU->buscarUsuario(nick);
	} catch (invalid_argument e) {
		throw invalid_argument(e); //se manda el invalid argument generado en buscarUsuario | albert
	}
	cU->setNickName(nick);
	cout << "Ingrese contrasenia: ";
	a= getch();
	while(a != 13){
		password.push_back(a);
		cout<<"*";
		a = getch();
	}
	cout<<endl;
	if (cU->chequearPassword(password)) {
		Sesion* sesion = Sesion::getInstancia();
		sesion->setNickName(nick);
		sesion->setAdmin(user->getAdmin());//#MODIF YA SE LE ASIGNA EL ADMIN
		cout << "Sesion iniciada" << endl;
	}
	else {
		cout << "Contraseña incorrecta" << endl;
		cout << "¿Desea intentarlo devuelta? [y/n]"	<< endl;
		char a;
		bool salgo = false;
		while (!salgo) {
			cin >> a;
			switch (a) {
				case 'y':
					cU->iniciarSesion(nick);
					salgo = true;
					break;
				case 'n':
					salgo = true;
					break;
				default:
					cout << "Opcion no valida" << endl;
					break;
			}
		}
	}
}

void ControlUsuario::mostrarComentarios(string titulo){
	ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
	mU ->mostrarComentarios(titulo);

}

ControlUsuario* ControlUsuario::getInstancia(){
	if (instancia == NULL)
		instancia = new ControlUsuario();
	return instancia;
}

void ControlUsuario::setNickName(string nick){
	this->nickName = nick;
}

void ControlUsuario::responderComentario(int id,string texto,string titulo){
	ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
	try {
		mU->responderComentario(id,texto,titulo);
	} catch (invalid_argument e){
		cout << e.what() << endl;
	}
}