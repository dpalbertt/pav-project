#include "Puntaje.h"

Puntaje::Puntaje(){}

Puntaje::~Puntaje(){}

Puntaje::Puntaje(int puntos, Pelicula* pelicula, Usuario* usuario){
	this->puntos = puntos;
	this->pelicula = pelicula;
	this->usuario = usuario;
}

int Puntaje::getPuntos(){
	return this->puntos;
}

void Puntaje::setPuntos(int puntos){
	this->puntos = puntos;
}

void Puntaje::setPelicula(Pelicula* pelicula){
	this->pelicula = pelicula;
}

void Puntaje::setUsuario(Usuario* usuario){
	this->usuario = usuario;
}

Pelicula* Puntaje::getPelicula(){
	return this->pelicula;
}

Usuario* Puntaje::getUsuario(){
	return this->usuario;
}

bool operator <(const Puntaje &p1, const Puntaje &p2){
	if (p1.puntos < p2.puntos)
		return true;
	else
		return false;
}