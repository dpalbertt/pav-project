#include "Cine.h"


Cine::Cine(){}

Cine::~Cine(){}

Cine::Cine(int id, Direccion direccion){
	this->id = id;
	this->direccion = direccion;
	map<int, Sala*> nada;
	this->salas = nada;
}

int Cine::getId(){
	return this->id;
}

Direccion Cine::getDireccion(){
	return this->direccion;
}

int Cine::idAuto = 0;

int Cine::getIdAuto(){
	idAuto++; 
	return idAuto; 
}

void Cine::setId(int id){
	this->id = id;
}

void Cine::setDireccion(Direccion direccion){
	this->direccion = direccion;
}

void Cine::setSalas(map<int, Sala*> salas){
	this->salas = salas;
}

map<int, Sala*> Cine::obtenerSalas(){
	return this->salas;
}

void Cine::addSala(Sala* sala){
	pair<int, Sala*> salas;
	salas.first = sala->getId();
	salas.second = sala;
	this->salas.insert(salas);
}

bool operator <(const Cine &p1, const Cine &p2){
	if (p1.id < p2.id)
		return true;
	else
		return false;
}