#ifndef DIRECCION_H
#define DIRECCION_H
#include <iostream>
using namespace std;

class Direccion {
private:
	string calle;
	int numero;
public:
	Direccion();
	~Direccion();
	Direccion(string,int);
	string getCalle();
	int getNumero();
	void setCalle(string);
	void setNumero(int);
	friend ostream& operator << (ostream&,const Direccion&);
};

#endif