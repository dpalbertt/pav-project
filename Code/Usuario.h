#ifndef USUARIO
#define USUARIO
#include <iostream>
#include <set>
#include "Comentario.h"

using namespace std;

class Usuario{
private:
	int id;
	string nickName;
	string contrasenia;
	string urlFoto;
	bool admin;
	set<Comentario> comentarios;
	//hay que agregar set de comentarios
public:
	Usuario();
	Usuario(string,string,string);
	string getNickName();
	string getContrasenia();
	string getUrlFoto();
	bool getAdmin();
	set<Comentario> getComentarios();
	void setComentarios(set<Comentario>);
	
	void addComentario(Comentario);
	void setAdmin(bool);
	void setNickName(string);
	void setContrasenia(string);
	void setUrlFoto(string);
	~Usuario();
	void quitarComentario(int);
	friend bool operator <(const Usuario &,const Usuario & );

};

#endif