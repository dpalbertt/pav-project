#ifndef CONTROLSALA_H
#define CONTROLSALA_H
#include "IControlSala.h"
#include <iostream>
using namespace std;

class ControlSala : public IControlSala {
private:
		ControlSala();
		~ControlSala();
		int capacidad;
		static ControlSala* instancia;
		int sala;
public:
	static ControlSala* getInstancia();
	void ingresarCapacidad(int); 
	void seleccionarSala(int);
	int getIdSala();

};

#endif