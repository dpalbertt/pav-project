#ifndef ICONTROLUSUARIO_H
#define ICONTROLUSUARIO_H
#include <iostream>
using namespace std;

class IControlUsuario{
public:
	virtual void iniciarSesion(string) = 0;
	virtual bool chequearPassword(string) = 0;
	virtual void mostrarComentarios(string) = 0 ;
	virtual void responderComentario(int,string,string) = 0;
};
//NO LLEVA .CPP NO TIENE CONSTRUCTOR NO TIENE NADA NO SIRVE PAR NADA
#endif
