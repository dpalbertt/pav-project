#ifndef CONTROLPELICULA_H
#define CONTROLPELICULA_H
#include "IControlPelicula.h"
//#include "DtComentario.h"
#include <iostream>
#include <set>
#include <map>
#include "DtSala.h"
//#include "DtCine.h"
//#include "Direccion.h"

using namespace std;

class ControlPelicula : public IControlPelicula {
private:
    ControlPelicula();
    ~ControlPelicula();
    static ControlPelicula* instancia;
    //atributos
    string titulo;
    
public:
    set<DtCine> seleccionarPelicula(string);
    set<DtPelicula> listarPeliculas();
    set<string> listarTituloPeliculas();
    void recordarPelicula(string); //que ondaaaaaa
    void confirmar();//elimina la pelicula
    void listarPuntajes(string);
    static ControlPelicula* getInstanciaPelicula();
    string getIdPelicula();
    void ingresarPuntaje(float);  
    void comentarPelicula(DtComentario*,string);
    DtInfoPelicula listarInfoPelicula(string);
    set<DtComentario> listarComentarios(string);
    void mostrarPuntajePromedio(string);

};

#endif
