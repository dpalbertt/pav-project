#ifndef FABRICA_H
#define FABRICA_H
#include "IControlCine.h"
#include "IControlUsuario.h"
#include "IControlReserva.h"
#include "IControlPelicula.h"
#include "IControlFuncion.h"
#include "IControlSala.h"
#include "ControlUsuario.h"
#include "ControlReserva.h"
#include "ControlPelicula.h"
#include "ControlFuncion.h"
#include "ControlCine.h"
#include "ControlSala.h"

class Fabrica {
private:
	static Fabrica* instancia;
	Fabrica();
public:
	static Fabrica* getInstancia();
	IControlCine* getIControlCine();
	IControlUsuario* getIControlUsuario();
	IControlPelicula* getIControlPelicula();
	IControlReserva* getIControlReserva();
	IControlFuncion* getIControlFuncion();
	IControlSala* getIControlSala();
	
	~Fabrica();
};

#endif
