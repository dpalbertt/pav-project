#ifndef RELOJ_H
#define RELOJ_H

#include <iostream>
#include <string.h>
using namespace std;

class Reloj{
private:
	Reloj();
	static Reloj* instancia;
	string fecha;
	/*string dia;
	string mes
	string anio;
	string hora;
	string minuto;
	string segundos;*/
public:
	~Reloj();
	static Reloj* getInstancia();
	/*void setDia(string);
	void setMes(string);
	void setAnio(string);
	void setHora(string);
	void setMinuto(string);
	void setSegundos(string);*/
	void setFecha(string,string,string,string,string,string);
	/*string getDia();
	string getMes();
	string getAnio();
	string getHora();
	string getMinuto();
	string getSegundos();*/
	string getFecha();
};

#endif