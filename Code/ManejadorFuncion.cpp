#include "ManejadorFuncion.h"
#include <stdexcept>
#include <typeinfo>

ManejadorFuncion::ManejadorFuncion(){}
ManejadorFuncion::~ManejadorFuncion(){}

ManejadorFuncion* ManejadorFuncion::instancia = NULL;

ManejadorFuncion* ManejadorFuncion::getManejadorFuncion(){
    if (instancia == NULL)
        instancia = new ManejadorFuncion();
    return instancia;
}

Funcion* ManejadorFuncion::buscarFuncion(int id){
    Funcion* func;
    bool found = false;
    for (auto iter : this->funciones){
        if (iter.first == id){
            found = true;
            func = iter.second;
        }
    }
    if (found) {
        return func;
    }
    else {
        //hay que throwear que no existe la funcion. pongo return null para que compile
        // return NULL;
        throw invalid_argument("No existe la funcion en el sistema");
    }
}

void ManejadorFuncion::agregarFuncion(Funcion funcion){
    Funcion* aux = new Funcion(funcion.getDia(), funcion.getId(), funcion.getHorario());
    aux->setPelicula(funcion.getPelicula());
    this->funciones.insert(pair<int, Funcion*>(funcion.getId(), aux));
}

map<int, Funcion*> ManejadorFuncion::obtenerFunciones(){
    return this->funciones;
}