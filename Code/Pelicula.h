#ifndef PELICULA_H
#define PELICULA_H
#include <iostream>
#include <map>
#include <set>
#define MAX_CINES 50

using namespace std;

class Cine; //forward declaration

class Pelicula{
	private:
		string titulo;
		string sinopsis;
		float puntajePromedio;
		string poster;
		map<int,Cine*> cines;
		set<int> idCines;
	public:
		Pelicula();
		~Pelicula();
		Pelicula(string, string, string);
		string getTitulo();
		string getSinopsis();
		string getPoster();
		void setTitulo(string);
		void setSinopsis(string);
		void setPoster(string);
		void setCines(map<int,Cine*>);
		void addCine(Cine*);
		void addIdCines(int);
		set<int> getIdCines();
		map<int,Cine*> obtenerCines();
		
		/*leandro*/float getPuntajePromedio();

		friend bool operator <(const Pelicula&,const Pelicula&);
		//void actualizarPuntajePromedio(); //todavia no, hay que arreglar el cpp de esta funcion
};


#endif
