#include "ManejadorPuntaje.h"


ManejadorPuntaje::ManejadorPuntaje(){}
ManejadorPuntaje::~ManejadorPuntaje(){}

ManejadorPuntaje* ManejadorPuntaje::instancia = NULL;

ManejadorPuntaje* ManejadorPuntaje::getManejadorPuntaje(){
	if (instancia == NULL)
		instancia = new ManejadorPuntaje();
	return instancia;
}

float ManejadorPuntaje::obtenerPuntajePelicula(string pelicula){
	float puntajePromedio = 0;
	float puntos = 0;
	float a = 0;
	for(auto aux : this->puntajes){
		Pelicula* pel = aux.getPelicula();
		string nomPelicula = pel->getTitulo();
		if (nomPelicula==pelicula){
			puntos += aux.getPuntos();
			a++;
		}
	}	
	puntajePromedio = puntos / a;
	return puntajePromedio;
}

void ManejadorPuntaje::ingresarPuntaje(Puntaje puntaje){
	this->puntajes.insert(puntaje);
}

multiset<Puntaje> ManejadorPuntaje::getPuntajes(){
	return this->puntajes;
}

void ManejadorPuntaje::quitarPuntaje(int aux){
	multiset<Puntaje>::iterator iter = this->puntajes.begin();
	for (int i = 0; i < aux; i++){
		iter++;
	}
	this->puntajes.erase(iter);
}