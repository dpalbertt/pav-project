#ifndef MANEJADORPELICULA_H
#define MANEJADORPELICULA_H
#include "Pelicula.h"
#include <iostream>
#include <set>

class ManejadorPelicula {
private:
	static ManejadorPelicula* instancia; //para singleton
	map<string, Pelicula*> peliculas;
	ManejadorPelicula();
	~ManejadorPelicula();
public:
	static ManejadorPelicula* getManejadorPelicula();
	void verInformacionPelicula(Pelicula);
	map<string, Pelicula*> obtenerPeliculas();
	Pelicula* buscarPelicula(string);
	void agregarPelicula(Pelicula);
	void quitar(Pelicula);
	//void agregarComentario(Usuario,Comentario);
};

#endif