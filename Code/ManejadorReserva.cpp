#include "ManejadorReserva.h"

ManejadorReserva::ManejadorReserva(){}
ManejadorReserva::~ManejadorReserva(){}

ManejadorReserva* ManejadorReserva::instancia = NULL;

ManejadorReserva* ManejadorReserva::getManejadorReserva(){
	if (instancia == NULL)
		instancia = new ManejadorReserva();
	return instancia;
}

void ManejadorReserva::agregarReserva(Reserva reserva){
	this->reservas.insert(reserva);
	cout<<"Reserva ingresada exitosamente.\n";
}

multiset<Reserva> ManejadorReserva::obtenerReservas(){
	return this->reservas;
}