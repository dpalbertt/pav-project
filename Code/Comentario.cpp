#include "Comentario.h"

Comentario::Comentario(){}

Comentario::~Comentario(){}

Comentario::Comentario(string nick, string texto, Pelicula* pelicula){
	this->nick = nick;
	this->texto = texto;
	this->pelicula = pelicula;
}


int Comentario::idAuto = 0;


string Comentario::getTexto(){
	return this->texto;
}

void Comentario::setIdAuto(){
	this->idAuto++;
	this->id = idAuto;
}

int Comentario::getId(){
	return this->id;
}

void Comentario::setTexto(string texto){
	this->texto = texto;
}

void Comentario::setPelicula(Pelicula* pelicula){
	this->pelicula = pelicula;
}

void Comentario::addRespuesta(Comentario respuestas){
	this->respuestas.insert(respuestas);
}

set<Comentario> Comentario::getRespuestas(){
	return this->respuestas;
}

Pelicula* Comentario::getPelicula(){
	return this->pelicula;
}

string Comentario::getNick(){
	return this->nick;
}

void Comentario::setNickName(string nick){
	this->nick = nick;
}

bool Comentario::getEsRespuesta(){
	return this->esRespuesta;
}

void Comentario::setEsRespuesta(bool respuesta){
	this->esRespuesta = respuesta;
}

void Comentario::setRespuestas(set<Comentario> respuestas){
	this->respuestas = respuestas;
}

bool operator <(const Comentario &p1, const Comentario &p2){
	if (p1.texto < p2.texto)
		return true;
	else
		return false;
}

