#include "Funcion.h"

Funcion::Funcion(){}
Funcion::~Funcion(){}

int Funcion::autoId = 0;
int Funcion::getIdAuto(){
	autoId++;
	return autoId;
}

Funcion::Funcion(Fecha fecha, int id, Horario horario){
    this->dia = fecha;
    this->id = id;
    this->horario = horario;
    this->pelicula = NULL;
}

void Funcion::setDia(Fecha fecha){
    this->dia = fecha;
}

void Funcion::setId(int id){
    this->id = id;
}

void Funcion::setHorario(Horario horario){
    this->horario = horario;
}

void Funcion::setPelicula(Pelicula* pelicula){
    this->pelicula = pelicula;
}

Fecha Funcion::getDia(){
    return this->dia;
}

int Funcion::getId(){
    return this->id;
}

Horario Funcion::getHorario(){
    return this->horario;
}

Pelicula* Funcion::getPelicula(){
    return this->pelicula;
}

bool operator <(const Funcion &p1, const Funcion &p2){
	if (p1.id < p2.id)
		return true;
	else
		return false;
}