#ifndef CONTROLUSUARIO_H
#define CONTROLUSUARIO_H
#include "IControlUsuario.h"

class ControlUsuario : public IControlUsuario {
public:
	bool chequearPassword(string);
	void iniciarSesion(string);
	~ControlUsuario();
	static ControlUsuario* getInstancia();
	void setNickName(string);
	string getNickname();
	void mostrarComentarios(string);
	void responderComentario(int,string,string);
private: //atributos que recuerda y constructores si es singleton
	string nickName;
	int idComentario;
	static ControlUsuario* instancia;
	ControlUsuario();

};

#endif