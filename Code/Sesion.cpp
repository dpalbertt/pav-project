#include "Sesion.h"
#include "ManejadorUsuario.h" //MODIF
#include <string.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

Sesion* Sesion::instancia = NULL;

Sesion::Sesion(){}
Sesion::~Sesion(){}
Sesion* Sesion::getInstancia(){
	if(instancia==NULL){
		instancia= new Sesion();
	}
	return instancia;
}

string Sesion::getNickName(){
	return this->nickName;
}

void Sesion::setNickName(string nick){
	this->nickName = nick;
}

bool Sesion::getAdmin(){
	return this->admin;
}

void Sesion::setAdmin(bool admin){//MODIF
	this->admin = admin;
}
