#ifndef DTCOMENTARIO_H
#define DTCOMENTARIO_H
#include <iostream>
#include <set>
#include "Comentario.h"

using namespace std;

class DtComentario {
private:
	string nickName;
	string texto;
	set<DtComentario> respuestas;
public:
	DtComentario();
	~DtComentario();
	DtComentario(string,string,set<DtComentario>);
	DtComentario(Comentario);
	string getNickName();
	string getTexto();
	void setNickName(string);
	void setTexto(string);
	set<DtComentario> getRespuestas();
	friend bool operator <(const DtComentario &,const DtComentario &);
	friend ostream& operator << (ostream&, DtComentario&);
};

#endif
