#ifndef MANEJADORRESERVA_H
#define MANEJADORRESERVA_H
#include "Reserva.h"
#include <iostream>
#include <set>

class ManejadorReserva {
private:
	static ManejadorReserva* instancia; //para singleton
	multiset<Reserva> reservas;
	ManejadorReserva();
	~ManejadorReserva();
public:
	static ManejadorReserva* getManejadorReserva();
	void agregarReserva(Reserva);
	multiset<Reserva> obtenerReservas();
};

#endif