#include "Reserva.h"
#include "Usuario.h"
#include "Cine.h"
#include "Funcion.h"
#include "ControlReserva.h"
#include "Pelicula.h"
#include "ManejadorReserva.h"
#include "ManejadorUsuario.h"
#include "ManejadorFuncion.h"
#include "Credito.h"
#include "Debito.h"
#include "Sesion.h"
#include <string>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <sstream>


using namespace std;


ControlReserva::ControlReserva(){}
ControlReserva::~ControlReserva(){}
ControlReserva* ControlReserva::control = NULL;

void ControlReserva::confirmar(){
	//llamo la sesion para conseguir el nick del usuario
	Sesion* mS = Sesion::getInstancia();
	string nick = mS->getNickName();

	//lo busco en la coleccion global
	ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
	Usuario* user = mU->buscarUsuario(nick);

	//busco la funcion en la coleccion global
	ManejadorFuncion* mF = ManejadorFuncion::getManejadorFuncion();
	Funcion* funcion = mF->buscarFuncion(this->idFuncion);

	//creo la reserva
	Reserva reserva;
	if (this->modopago != 0) //es un enumerado
		reserva = Credito(this->verPrecioTotal(), this->asientos, funcion, user, this->financiera, this->descuento);
	else
		reserva = Debito(this->verPrecioTotal(), this->asientos, funcion, user, this->nombreBanco); 
	
	//la agrego en la coleccion global
	ManejadorReserva* mR = ManejadorReserva::getManejadorReserva();
	mR->agregarReserva(reserva);


}

float ControlReserva::verPrecioTotal(){
	int precioEntrada = 180; //costo de la entrada que hay que discutir
	return this->asientos * precioEntrada -(this->asientos * precioEntrada * (atof(this->descuento.c_str())/100));
	/*if ((atoi(this->descuento.c_str()) < 30) && (atoi(this->descuento.c_str()) > 10))
		return this->asientos * precioEntrada * (atoi(this->descuento.c_str())/100); //ver si this->descuento es null, stoi tiene que devolver 0
	else
		return this->asientos * precioEntrada;*/
}

float ControlReserva::ingresarFinanciera(string financiera){
	int random;
	srand(time(NULL));
	random = rand() % (41);
	stringstream ss;
	ss<<random;
	this->financiera = financiera;
	this->descuento = ss.str(); //hacer un random entre 10 y 30
	return atoi(this->descuento.c_str()); 
}
void ControlReserva::ingresarNomBanco(string nomBanco){
	this->nombreBanco = nomBanco;
	this->descuento = "0";
}

void ControlReserva::ingresarModoPago(int a){
	if(a == 1){
		this->modopago = credito;
	}else{
		this->modopago = debito;
	}
}

void ControlReserva::ingresarCantidadAsientos(int cantidad){
	this->asientos = cantidad;
}
ControlReserva* ControlReserva::getInstancia(){
	if (control == NULL)
		control = new ControlReserva();
	return control;
}

void ControlReserva::setIdFuncion(int id){
	this->idFuncion = id;
}
