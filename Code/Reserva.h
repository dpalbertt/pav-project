#ifndef RESERVA_H
#define RESERVA_H

#include "Funcion.h"
#include "Usuario.h"

using namespace std;

class Reserva {
private:
	float costo;
	int cantEntradas;
	Funcion* funcion;
	Usuario* usuario;
public:
	Reserva();
	virtual ~Reserva();
	Reserva(float, int, Funcion*, Usuario*);
	void setCosto(float);
	void setCantEntradas(int);
	void setFuncion(Funcion*);
	void setUsuario(Usuario*);
	float getCosto();
	int getCantEntradas();
	Usuario* getUsuario();
	Funcion* getFuncion();
	friend bool operator <(const Reserva &,const Reserva &);
};

#endif