#ifndef ICONTROLFUNCION_H
#define ICONTROLFUNCION_H

#include <iostream>
#include "Horario.h"
#include "Fecha.h"
#include "DtFuncion.h"
using namespace std;

class IControlFuncion {
public:
    virtual void seleccionarFuncion(int) = 0;
    virtual void altaFuncion(Horario, Fecha) = 0;
    virtual void ingresarHorario() = 0;
    virtual Horario getHorario() = 0;
    virtual Fecha getFecha() = 0;
    virtual int getId() = 0;
};

#endif
