#ifndef HORARIO_H
#define HORARIO_H
#include <iostream>
using namespace std;

class Horario{
private:
	string horaComienzo;
	string horaFin;
public:
	Horario();
	~Horario();
	Horario(string,string);
	string getHoraComienzo();
	string getHoraFin();
	void setHoraComienzo();
	void setHoraFin();

	friend ostream& operator << (ostream&, Horario&);
};

#endif
