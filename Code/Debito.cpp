#include "Debito.h"

Debito::Debito(){}
Debito::~Debito(){}
Debito::Debito(float costo, int asientos, Funcion* funcion, Usuario* usuario, string banco):Reserva(costo, asientos, funcion, usuario){
	this->banco = banco;
}

void Debito::setBanco(string banco){
	this->banco = banco;
}

string Debito::getBanco(){
	return this->banco;
}