#include "DtPuntaje.h"

DtPuntaje::DtPuntaje(){}
DtPuntaje::~DtPuntaje(){}
DtPuntaje::DtPuntaje(string nickName, float puntos){
	this->nickName = nickName;
	this->puntos = puntos;
}

string DtPuntaje::getNickName(){
	return this->nickName;
}

float DtPuntaje::getPuntos(){
	return this->puntos;
}

ostream& operator <<(ostream& salida, DtPuntaje& dtF){
	cout << "Nick: " << dtF.nickName <<endl;
	cout << "Puntaje: "<< dtF.puntos <<endl;
	return salida;
}