#include "Fabrica.h"

Fabrica* Fabrica::instancia = NULL;

Fabrica::Fabrica(){}

IControlCine* Fabrica::getIControlCine(){
	return ControlCine::getInstancia();
}

IControlUsuario* Fabrica::getIControlUsuario(){
	return ControlUsuario::getInstancia();
}

IControlPelicula* Fabrica::getIControlPelicula(){
	return ControlPelicula::getInstanciaPelicula();
}	

IControlReserva* Fabrica::getIControlReserva(){
	return ControlReserva::getInstancia();
}

IControlFuncion* Fabrica::getIControlFuncion(){
	return ControlFuncion::getInstancia();
}

Fabrica* Fabrica::getInstancia(){
	if (instancia == NULL)
		instancia = new Fabrica();
	return instancia;
}

IControlSala* Fabrica::getIControlSala(){
	return ControlSala::getInstancia();
}
Fabrica::~Fabrica(){}

		