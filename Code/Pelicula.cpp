#include "Pelicula.h"
#include <map>
#include <set>
#include "Cine.h"

Pelicula::Pelicula(){}

Pelicula::~Pelicula(){}

Pelicula::Pelicula(string titulo, string sinopsis, string poster){
	this->titulo = titulo;
	this->sinopsis = sinopsis;
	this->poster = poster;
	map<int,Cine*> nada;
	this->cines = nada;
	set<int> nadas;
	this->idCines = nadas;
}

string Pelicula::getTitulo(){
	return this->titulo;
}

string Pelicula::getSinopsis(){
	return this->sinopsis;
}


string Pelicula::getPoster(){
	return this->poster;
}

void Pelicula::setTitulo(string titulo){
	this->titulo = titulo;
}

void Pelicula::setSinopsis(string sinopsis){
	this->sinopsis = sinopsis;
}

void Pelicula::setPoster(string poster){
	this->poster = poster;
}

void Pelicula::addCine(Cine* cine){
	pair<int,Cine*> dummy;
	int id = cine->getId();
	dummy.first = id;
	dummy.second = cine;
	this->cines.insert(dummy);
}

void Pelicula::addIdCines(int id){
	this->idCines.insert(id);
}

set<int> Pelicula::getIdCines(){
	return this->idCines;
}

void Pelicula::setCines(map<int,Cine*> cines){
	this->cines = cines;
}

map<int,Cine*> Pelicula::obtenerCines(){
	return this->cines;
}

/*set<DtPuntaje*> Pelicula::getPuntajes(){
	return this->puntajes;
}
//no estan en el .h | albert
set<Comentario*> Pelicula::getComentarios(){
	return this->comentarios;
}*/

/**/ float Pelicula::getPuntajePromedio(){
	return this->puntajePromedio;
}

bool operator <(const Pelicula& p1 , const Pelicula& p2){
	if(p1.puntajePromedio < p2.puntajePromedio){
		return true;
	}
	return false;
}

/*leadro*//*void Pelicula::actualizarPuntajePromedio(){
	int a = 0;
	int puntaje= 0;
	set<DtPuntaje*>::iterator itP = this->puntajes.begin();
	for(itP;itP!=itP.end();++itP){
	a++;
	puntaje = puntaje + *itP->getPuntos();
	}
	int puntajePromedio = puntaje/a;
	this->puntajePromedio = puntajePromedio;
} */







