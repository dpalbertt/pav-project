#ifndef DTFUNCION_H
#define DTFUNCION_H
#include "Fecha.h"
#include "Horario.h"
#include <iostream>
using namespace std;

class DtFuncion {
private:
	Fecha dia;
	int id;
	Horario horario;
public:
	DtFuncion();
	~DtFuncion();
	DtFuncion(Fecha,int,Horario);
	Fecha getDia();
	int getId();
	Horario getHorario();
	void setDia(Fecha);
	void setId(int);
	void setHorario(Horario);

	friend ostream& operator << (ostream&, DtFuncion&);
	friend bool operator <(const DtFuncion &,const DtFuncion &);
};

#endif