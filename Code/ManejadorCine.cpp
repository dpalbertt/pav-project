#include "Cine.h"
#include "ManejadorCine.h"
#include <list>
#include <map>
#include <set>

using namespace std;

ManejadorCine::ManejadorCine(){}

ManejadorCine* ManejadorCine::instancia = NULL;

ManejadorCine* ManejadorCine::getInstancia(){
	if(instancia==NULL){
		instancia = new ManejadorCine();
	}
	return instancia;
}

void ManejadorCine::agregarCine(Cine cine){
	Cine* aux = new Cine(cine.getId(), cine.getDireccion());
	aux->setSalas(cine.obtenerSalas());
	this->cines.insert(pair<int, Cine*>(cine.getId(), aux));
}

map<int, Cine*> ManejadorCine::getCines(){
	return this->cines;
}

Cine* ManejadorCine::buscarCine(int id){
	bool found = false;
	Cine* cine;
	map<int, Cine*>::iterator iter = this->cines.begin();
	while (iter != this->cines.end() && !found){
		if (iter->first == id){
			found = true;
			cine = iter->second;
		}
		iter++;
	}	
	if (found)
		return cine;
	else
		throw invalid_argument("No existe el cine en el sistema");
}

ManejadorCine::~ManejadorCine(){}