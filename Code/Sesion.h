#ifndef SESION_H
#define SESION_H
#include <iostream>
using namespace std;
#include <stdlib.h>

class Sesion{
private:
	string nickName;
	bool admin;
	static Sesion* instancia;//constructor es privado por  que es singleton
	Sesion();
public:
	~Sesion();
	static Sesion* getInstancia();
	string getNickName();
	void setNickName(string);
	bool getAdmin();
	void setAdmin(bool);
};

#endif