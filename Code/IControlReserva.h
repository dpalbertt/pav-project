#ifndef ICONTROLRESERVA_H
#define ICONTROLRESERVA_H
#include "TipoPago.h"
#include <iostream>
#include "DtInfoPelicula.h"

using namespace std;

class IControlReserva {
public:
	virtual void confirmar() = 0;
	virtual float verPrecioTotal() = 0;
	virtual float ingresarFinanciera(string) = 0;
	virtual void ingresarNomBanco(string) = 0;
	virtual void ingresarModoPago(int) = 0;
	virtual void ingresarCantidadAsientos(int) = 0;
	virtual void setIdFuncion(int) = 0;
};

#endif