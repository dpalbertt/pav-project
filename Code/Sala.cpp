#include "Sala.h"
#include <map>

Sala::Sala(){}

Sala::~Sala(){}

Sala::Sala(int id, int capacidad){
	this->id = id;
	this->capacidad = capacidad;
	map<int,Funcion*> nada;
	this->funciones = nada;
}


int Sala::idAuto = 0;

int Sala::getIdAuto(){
	idAuto++;
	return idAuto;
}

int Sala::getId(){
	return this->id;
}

int Sala::getCapacidad(){
	return this->capacidad;
}

void Sala::setId(int id){
	this->id = id;
}

void Sala::setCapacidad(int capacidad){
	this->capacidad = capacidad;
}

void Sala::addFuncion(Funcion* funciones){
	int id = funciones->getId();
	pair<int,Funcion*> funcion;
	funcion.second = funciones;
	funcion.first = id;
	this->funciones.insert(funcion);
}

map<int,Funcion*> Sala::obtenerFunciones(){
	return this->funciones;
}

void Sala::quitarFuncion(int id){
	map<int, Funcion*>::iterator ptr = this->funciones.begin();
	map<int, Funcion*>::iterator borrar;
	bool found = false;
	Funcion* aux;
	for (auto iter : this->funciones){
		aux = iter.second;
		if (aux->getId() == id && !found){
			found = true;
			borrar = ptr;
		}
		ptr++;
	}
	if (found){
		this->funciones.erase(borrar);
		delete aux; //libero la memoria
	}
}

bool operator <(const Sala &p1, const Sala &p2){
	if (p1.id < p2.id)
		return true;
	else
		return false;
}