#ifndef MANEJADORFUNCION_H
#define MANEJADORFUNCION_H
#include "Funcion.h"
#include <iostream>
#include <set>

class ManejadorFuncion {
private:
    static ManejadorFuncion* instancia; //para singleton
    map<int, Funcion*> funciones;
    ManejadorFuncion();
    ~ManejadorFuncion();
public:
    static ManejadorFuncion* getManejadorFuncion();
    Funcion* buscarFuncion(int);
    void agregarFuncion(Funcion);
    map<int, Funcion*> obtenerFunciones();
};

#endif