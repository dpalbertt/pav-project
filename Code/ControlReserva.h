#ifndef CONTROLRESERVA_H
#define CONTROLRESERVA_H

#include <iostream>
#include "IControlReserva.h"
using namespace std;

class ControlReserva : public IControlReserva {
	public:
		void confirmar();
		float verPrecioTotal();
		float ingresarFinanciera(string);
		void ingresarNomBanco(string);
		void ingresarModoPago(int);
		void ingresarCantidadAsientos(int);
		static ControlReserva* getInstancia();
		void setIdFuncion(int);

	private:
		int idFuncion;
		int asientos;
		string nombreBanco;
		string financiera;
		tipoPago modopago;
		string descuento;
		static ControlReserva* control;
		ControlReserva();
		~ControlReserva();
		
};

#endif