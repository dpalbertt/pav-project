#include "Horario.h"

Horario::Horario(){}
Horario::~Horario(){}
Horario::Horario(string horaComienzo,string horaFin){
	this->horaComienzo = horaComienzo;
	this->horaFin = horaFin;
}

string Horario::getHoraComienzo(){
	return this->horaComienzo;
}

string Horario::getHoraFin(){
	return this->horaFin;
}

ostream& operator <<(ostream& salida, Horario& hor){
	cout<<"Comienzo: "<<hor.horaComienzo<<endl;
	cout<<"Fin: "<<hor.horaFin<<endl;
	return salida;

}