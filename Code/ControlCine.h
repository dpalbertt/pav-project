#ifndef CONTROLCINE_H
#define CONTROLCINE_H
#include "IControlCine.h"
#include <set>
#include <iostream>
using namespace std;

class ControlCine : public IControlCine {
private:
	ControlCine();
	static ControlCine* instancia;
	string direccion;
	int cineId;
public:
	static ControlCine* getInstancia();
	set<DtFuncion> selectCine(int);
	set<DtSala> seleccionarCine(int);
	void altaCine(Direccion);
	void agregarSalas(set<int>);
	void ingresarDireccion(Direccion);
	int getCineId();
	~ControlCine();
};

#endif
