#ifndef CREDITO_H
#define CREDITO_H
#include "Reserva.h"

class Credito : public Reserva {
private:
	string porcentajeDescuento;
	string financiera;
public:
	Credito();
	~Credito();
	Credito(float, int, Funcion*, Usuario*, string, string);
	void setPorcentajeDescuento(string);
	void setFinanciera(string);
	string getPorcentajeDescuento();
	string getFinanciera();
};

#endif