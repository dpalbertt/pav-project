#include "DtPelicula.h"

DtPelicula::DtPelicula(){}
DtPelicula::~DtPelicula(){}
DtPelicula::DtPelicula(string titulo,string sinopsis,string poster){
	this->titulo = titulo;
	this->sinopsis = sinopsis;
	this->poster = poster;
}

string DtPelicula::getTitulo(){
	return this->titulo;
}

string DtPelicula::getSinopsis(){
	return this->sinopsis;
}

string DtPelicula::getPoster(){
	return this->poster;
}

/**/float DtPelicula::getPuntajePromedio(){
	return this->puntajePromedio;
}

bool operator <(const DtPelicula &p1,const DtPelicula &p2){
	if (p1.titulo < p2.titulo)
		return true;
	else
		return false;
}
