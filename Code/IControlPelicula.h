#ifndef ICONTROLPELICULA_H
#define ICONTROLPELICULA_H
#include "DtCine.h"
#include "DtPelicula.h"
#include "DtComentario.h"
#include "DtPuntaje.h"
#include <iostream>
#include <set>
#include "DtInfoPelicula.h"
using namespace std;

class IControlPelicula {
public:
    virtual set<DtCine> seleccionarPelicula(string) = 0;
    virtual set<DtPelicula> listarPeliculas() = 0;
    virtual set<string> listarTituloPeliculas() = 0;
    virtual void listarPuntajes(string) = 0;
    virtual void recordarPelicula(string) = 0;
    virtual void confirmar() = 0;//elimina la pelicula
    virtual void ingresarPuntaje(float) = 0;
    virtual void comentarPelicula(DtComentario*,string) = 0;
    virtual DtInfoPelicula listarInfoPelicula(string) = 0;
    virtual set<DtComentario> listarComentarios(string) = 0;
    virtual void mostrarPuntajePromedio(string) = 0;
 
};

#endif
