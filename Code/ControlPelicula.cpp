#include "ControlPelicula.h"
#include "ManejadorPelicula.h"
#include "Cine.h"
//#include "Pelicula.h"
#include "ManejadorPuntaje.h"
//#include "Comentario.h"
#include "Sesion.h"
//#include "Usuario.h"
//#include "ControlUsuario.h"
#include "DtPelicula.h"
#include "ManejadorUsuario.h"
#include "ManejadorCine.h"
#include <stdexcept>
#include <iomanip>

ControlPelicula::ControlPelicula(){}
ControlPelicula::~ControlPelicula(){}

ControlPelicula* ControlPelicula::instancia = NULL;

/* Si no andan los for_each, usar:
Ejemplo:
for (set<Pelicula*>::iterator iter = mP->obtenerPeliculas().begin(), mP->obtenerPeliculas().end(), ++iter){} 
Despues hay que sacar la funcion anonima y ponerla en el cuerpo del for.
*/



set<DtCine> ControlPelicula::seleccionarPelicula(string pelId/*titulo*/){
    bool found = false;
    this->titulo = pelId;
    set<DtCine> cines; //borre new | albert 16/6
    ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
    try {
        Pelicula* peli = mP->buscarPelicula(pelId);
        if (peli->obtenerCines().empty())
            throw invalid_argument("No hay cines para esta pelicula");
        else {
            for (auto f : peli->obtenerCines()){
                ManejadorCine* mC = ManejadorCine::getInstancia();
                Cine* cine = f.second;
                DtCine dtC = DtCine(cine->getId(), cine->getDireccion());
                cines.insert(dtC);
            }
            //agregue metodo buscarPelicula
            return cines;
        }
    } catch (invalid_argument e){
        throw invalid_argument(e.what());
    }

}

set<DtPelicula> ControlPelicula::listarPeliculas(){
    set<DtPelicula> peliculas;
    ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
    for (auto aux : mP->obtenerPeliculas()){
        Pelicula* pel = aux.second;
        DtPelicula dtP = DtPelicula(pel->getTitulo(), pel->getSinopsis(), pel->getPoster());
        peliculas.insert(dtP);
    }
    return peliculas;
}

set<string> ControlPelicula::listarTituloPeliculas(){
    set<string> titulos;
    ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
    for (auto f: mP->obtenerPeliculas()){
        titulos.insert(f.second->getTitulo());
    }
    return titulos;
}

void ControlPelicula::recordarPelicula(string pelId){ //cambio de nombre, error 'de sobrecarga' | albert 19/6
    this->titulo = pelId;
}

void ControlPelicula::confirmar(){
    //confirma que elimina la pelicula
    bool found = false;
    set<Cine> cines;
    ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
    try {
        Pelicula* pel = mP->buscarPelicula(this->titulo); 
        for (auto iterC : pel->obtenerCines()){
            Cine* cine = iterC.second;
            for (auto sala : cine->obtenerSalas()){
                for (auto iterF : sala.second->obtenerFunciones()){
                    Funcion* funcion = iterF.second;
                    Pelicula* peliF = funcion->getPelicula();
                    if (peliF->getTitulo() == pel->getTitulo()){ //deberia andar la comparacion, ya que cno sobrecargue == y compara direcciones de memoria
                        sala.second->quitarFuncion(funcion->getId());
                        //quito la funcion del map de esa sala
                    }
                }
            }
        }
        //despues de quitarle el puntero a cada funcion, quito la pelicula de la coleccion global
        ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
        bool iterar = true;
        for (auto iterU : mU->getUsuarios()){
            Usuario* user = iterU.second;
            set<Comentario>::iterator auxCom = user->getComentarios().begin();
            while (iterar){
                int contador = 0;
                for (auto aux : user->getComentarios()){
                    if (aux.getPelicula()->getTitulo() == this->titulo){
                        found = true;
                    }
                    if (!found)
                        ++contador;
                }
                if (found)
                    user->quitarComentario(contador);
                else
                    iterar = false;
            }
        }
        //Ya elimine los comentarios relacionados a esa pelicula
        //Ahora elimino los puntajes
        ManejadorPuntaje* manejaPunt = ManejadorPuntaje::getManejadorPuntaje();
        iterar = true;
        while (iterar){
            found = false;
            int contador = 0;
            for (auto f : manejaPunt->getPuntajes()){
                if (f.getPelicula()->getTitulo() == this->titulo){
                    found = true;
                }
                if (!found)
                    contador++;
            }
            if (found)
                manejaPunt->quitarPuntaje(contador);
            else
                iterar = false;
        }
        //elimine los puntajes de esa pelicula
        mP->quitar(*pel);
        cout << "Pelicula eliminada con exito" << endl;
        cout << endl;
    } catch (invalid_argument e){
        throw invalid_argument(e.what()); // devolviendo error al main
    }
}

void ControlPelicula::listarPuntajes(string titulo){
    ManejadorPuntaje* mP = ManejadorPuntaje::getManejadorPuntaje();
    multiset<Puntaje> puntajes = mP->getPuntajes();

    for (auto aux : puntajes){

       if (aux.getPelicula()->getTitulo() == titulo){
            DtPuntaje punt = DtPuntaje(aux.getUsuario()->getNickName(), aux.getPuntos());
            cout <<fixed<<setprecision(0)<< punt << endl;
        }
    }
    cout << endl;
} 

void ControlPelicula::mostrarPuntajePromedio(string titulo){
    ManejadorPuntaje* mP = ManejadorPuntaje::getManejadorPuntaje();
    multiset<Puntaje> puntajes = mP->getPuntajes();
    float puntajePromedio = 0;
    float cantidadUsuarios= 0;
    for (auto aux : puntajes){
       if (aux.getPelicula()->getTitulo() == titulo){
           // DtPuntaje punt = DtPuntaje(aux.getUsuario()->getNickName(), aux.getPuntos());
            puntajePromedio += aux.getPuntos();
            cantidadUsuarios++;
        }
    }
    cout<<fixed<<setprecision(2)<<puntajePromedio/cantidadUsuarios<<"("<<fixed<<setprecision(0)<<cantidadUsuarios<<" Usuarios)\n";
    cout << endl;

} 

ControlPelicula* ControlPelicula::getInstanciaPelicula(){
    if (instancia == NULL)
        instancia = new ControlPelicula();
    return instancia;
}

string ControlPelicula::getIdPelicula(){
    return this->titulo;
}

void ControlPelicula::ingresarPuntaje(float puntaje){
    try {
        ManejadorPuntaje* mP = ManejadorPuntaje::getManejadorPuntaje();
        ManejadorPelicula* mPel = ManejadorPelicula::getManejadorPelicula();
        ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
        Sesion* sesion= Sesion::getInstancia();
        Pelicula* pel = mPel->buscarPelicula(this->titulo);
        string nick = sesion->getNickName();
        Usuario* user = mU->buscarUsuario(nick);
        Puntaje puntajeAux = Puntaje(puntaje,pel,user);
        mP->ingresarPuntaje(puntajeAux); 
    } catch (invalid_argument e){
        throw invalid_argument(e.what()); //se lo mando por si no existe la pelicula
    }
}

void ControlPelicula::comentarPelicula(DtComentario* comentario,string titulo){
    ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
    ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
    Sesion* sesion= Sesion::getInstancia();
    string nick = sesion->getNickName();
    Usuario* user = mU->buscarUsuario(nick); //no precisa try catch porque es el que esta iniciado sesion
    Pelicula* pel= mP->buscarPelicula(titulo); //capaz que hay que meter try catch
    string a= comentario->getTexto();
    Comentario coment = Comentario(nick,a,pel);
    cout<<endl<<"pelicula es "<<coment.getPelicula()->getTitulo()<<endl;
    cout<<endl<<"El comnteario es "<<coment.getTexto()<<endl;
    mU->agregarComentario(user,coment);
}

DtInfoPelicula ControlPelicula::listarInfoPelicula(string titulo){
    ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
    Pelicula* peli = mP->buscarPelicula(titulo);
    DtInfoPelicula devolver = DtInfoPelicula(peli->getPoster(), peli->getSinopsis());
    return devolver;
}

set<DtComentario> ControlPelicula::listarComentarios(string titulo){
    set<DtComentario> salida;
    ManejadorUsuario* mU = ManejadorUsuario::getInstancia();
    map<string, Usuario*> usuarios = mU->getUsuarios();
    for(auto user : usuarios){
        set<Comentario> coso = user.second->getComentarios();
        for (auto aux : coso){
            Pelicula* peli = aux.getPelicula();
            if(peli->getTitulo()==titulo){
                DtComentario a = DtComentario(); //listar comentarios no se usa al final
                salida.insert(a);
            }
        }
    }
    return salida;
}

