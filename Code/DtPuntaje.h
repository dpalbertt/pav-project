#ifndef DTPUNTAJE_H
#define DTPUNTAJE_H

#include <iostream>
using namespace std;

class DtPuntaje{
private:
	string nickName;
	float puntos;
public:
	DtPuntaje();
	~DtPuntaje();
	DtPuntaje(string,float);
	string getNickName();
	float getPuntos();
	void setNickName(string);
	void setPuntos(float);
	friend ostream& operator << (ostream&, DtPuntaje&);
};

#endif