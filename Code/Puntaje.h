#ifndef PUNTAJE_H
#define PUNTAJE_H
#include "Pelicula.h"
#include "Usuario.h"

using namespace std;

class Puntaje{
	private:
		int puntos;
		Pelicula* pelicula;
		Usuario* usuario;
	public:
		Puntaje();
		~Puntaje();
		Puntaje(int, Pelicula*, Usuario*);
		int getPuntos();
		void setPuntos(int);
		void setPelicula(Pelicula*);
		void setUsuario(Usuario*);
		Pelicula* getPelicula();
		Usuario* getUsuario();
		friend bool operator <(const Puntaje &,const Puntaje &);
};


#endif