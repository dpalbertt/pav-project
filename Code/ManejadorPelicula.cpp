#include "ManejadorPelicula.h"
#include <stdexcept>
#include <map>
#include <set>
ManejadorPelicula::ManejadorPelicula(){}
ManejadorPelicula::~ManejadorPelicula(){}

ManejadorPelicula* ManejadorPelicula::instancia = NULL;

ManejadorPelicula* ManejadorPelicula::getManejadorPelicula() {
	if (instancia == NULL)
		instancia = new ManejadorPelicula();
	return instancia;
}

void ManejadorPelicula::verInformacionPelicula(Pelicula pelicula) {
	// this->reservas.insert(reserva);
}

map<string, Pelicula*> ManejadorPelicula::obtenerPeliculas(){
	return this->peliculas;
}

Pelicula* ManejadorPelicula::buscarPelicula(string titulo){ /* cambiado a set en lugar de map | albert */
	bool found = false;
	Pelicula* peli;
	map<string, Pelicula*>::iterator iter = this->peliculas.begin();
	while (iter != this->peliculas.end() && !found){
		if (iter->first == titulo){
			peli = iter->second;
			found = true;
		}
		iter++;
	}
	if (!found)
		throw invalid_argument("No se encontró la pelicula");
	return peli;
}

void ManejadorPelicula::quitar(Pelicula pel){
	map<string, Pelicula*>::iterator borrar = this->peliculas.begin();
	bool found = false;
	Pelicula* ptrPel;
	for (auto iter : this->peliculas){
		if (iter.first == pel.getTitulo()){
			ptrPel = iter.second;
			found = true;
		}
		if (!found)
			borrar++;
	}
	delete ptrPel;
	this->peliculas.erase(borrar);
}

void ManejadorPelicula::agregarPelicula(Pelicula pel){
	Pelicula* aux = new Pelicula(pel.getTitulo(), pel.getSinopsis(), pel.getPoster());
	aux->setCines(pel.obtenerCines());
	this->peliculas.insert(pair<string, Pelicula*>(pel.getTitulo(),aux));
}