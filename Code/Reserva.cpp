#include "Reserva.h"

Reserva::Reserva(){}
Reserva::~Reserva(){}
Reserva::Reserva(float costo, int entradas, Funcion* funcion, Usuario* usuario){
	this->costo = costo;
	this->cantEntradas = entradas;
	this->funcion = funcion;
	this->usuario = usuario;
}

void Reserva::setCosto(float costo){
	this->costo = costo;
}

void Reserva::setCantEntradas(int entradas){
	this->cantEntradas = entradas;
}

void Reserva::setFuncion(Funcion* funcion){
	this->funcion = funcion;
}

void Reserva::setUsuario(Usuario* usuario){
	this->usuario = usuario;
}

float Reserva::getCosto(){
	return this->costo;
}

int Reserva::getCantEntradas(){
	return this->cantEntradas;
}

Usuario* Reserva::getUsuario(){
	return this->usuario;
}

Funcion* Reserva::getFuncion(){
	return this->funcion;
}

bool operator <(const Reserva &p1, const Reserva &p2){
	if (p1.costo < p2.costo)
		return true;
	else
		return false;
}