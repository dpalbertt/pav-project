#include "ControlFuncion.h"
#include "ControlCine.h"
#include "ControlPelicula.h"
#include "ControlSala.h"
#include "ManejadorCine.h"
#include "ManejadorPelicula.h"
#include <iostream>
using namespace std;

ControlFuncion* ControlFuncion::control = NULL;


ControlFuncion::ControlFuncion(){}
ControlFuncion::~ControlFuncion(){}

void ControlFuncion::seleccionarFuncion(int id){
    ManejadorFuncion* manejador = ManejadorFuncion::getManejadorFuncion();
    manejador->buscarFuncion(id);
    this->id = id;
}

void ControlFuncion::altaFuncion(Horario horario, Fecha fecha){
	
    ControlPelicula* conP = ControlPelicula::getInstanciaPelicula();
    ManejadorPelicula* mP = ManejadorPelicula::getManejadorPelicula();
    Pelicula* pel = mP->buscarPelicula(conP->getIdPelicula());

    ManejadorFuncion* manejador = ManejadorFuncion::getManejadorFuncion();
    Funcion funcion = Funcion(fecha, Funcion::getIdAuto(), horario);
    funcion.setPelicula(pel);
    manejador->agregarFuncion(funcion);

    ControlCine* contCine = ControlCine::getInstancia();
    ControlSala* contSala = ControlSala::getInstancia();
    bool found = false;
    Cine* cine;
    for (auto iterC : pel->obtenerCines()){
        if (iterC.second->getId() == contCine->getCineId()){ //si es el cine que seleccione
            cine = iterC.second;
            found = true;
        }
    }
    //encuentro el cine
    Sala* sala;
    for (auto iter : cine->obtenerSalas()){
        if (iter.first == contSala->getIdSala()){
            sala = iter.second;
            Funcion* aux = manejador->buscarFuncion(funcion.getId());
            sala->addFuncion(aux);
        }
    }
    cout << "Funcion agregada con exito" << endl;
}

ControlFuncion* ControlFuncion::getInstancia(){
    if (control == NULL)
        control = new ControlFuncion();
    return control;
}

void ControlFuncion::ingresarHorario(){
    cout << "Ingrese hora de inicio" << endl;
    string inicio;
    cin >> inicio;
    cout << "Ingrese hora de fin" << endl;
    string fin;
    cin >> fin;
    Horario horario = Horario(inicio, fin);
    cout << "Ingrese dia de la funcion" << endl;
    int dia;
    cin >> dia;
    cout << "Ingrese mes de la funcion" << endl;
    int mes;
    cin >> mes;
    cout << "Ingrese anio de la funcion" << endl;
    int anio;
    cin >> anio;
    Fecha fecha = Fecha(dia, mes, anio);
    this->horario = horario;
    this->fecha = fecha;
}

Horario ControlFuncion::getHorario(){
    return this->horario;
}

Fecha ControlFuncion::getFecha(){
    return this->fecha;
}

void ControlFuncion::setId(int id){
    this->id = id;
}

int ControlFuncion::getId(){
    return this->id;
}