#define MAX_SALAS 10
#include "Fabrica.h"
#include "IControlFuncion.h"
#include "IControlPelicula.h"
#include "IControlReserva.h"
#include "IControlUsuario.h"
#include "IControlSala.h"
#include <iostream>
#include <stdexcept>
#include <string.h>
#include <list>
#include "Sesion.h"
#include "Reloj.h"
#include "Base.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
using namespace std;

Fabrica *fabrica;
IControlCine *iconC;
IControlFuncion *iconF;
IControlPelicula *iconP;
IControlReserva *iconR;
IControlUsuario *iconU;
IControlSala *iconS;
Reloj* reloj;
Base* base;

void menu(){
	
	cout<<"\nBienvenido.\n\nElija una de las siguientes opciones:\n\n";
	cout<<"1)Iniciar Sesion.\n";
	cout<<"2)Alta Cine.\n";
	cout<<"3)Alta Funcion.\n";
	cout<<"4)Crear Reserva.\n";
	cout<<"5)Puntuar Pelicula.\n";
	cout<<"6)Comentar Pelicula.\n";
	cout<<"7)Eliminar Pelicula.\n";
	cout<<"8)Ver Informacion de Pelicula.\n";
	cout<<"9)Ver Comentarios y Puntajes de Pelicula.\n";
	cout<<"10)Consultar fecha.\n";
	cout<<"11)Modificar fecha\n";
	cout<<"12)Salir.\n";
}

void cargarDatos(){
	Base* base = Base::getBase();
	base->crearDatos();
}


void modificarFecha(string fecha, string horario){
	string dia;
	string mes;
	string anio;
	string hora;
	string minuto;
	string segundos;
	int a = fecha.length();
	dia = fecha.substr(0,2);
	mes = fecha.substr(3,2);
	anio = fecha.substr(6,4);
	hora = horario.substr(0,2);
	minuto = horario.substr(3,2);
	segundos = horario.substr(6,2);
	reloj->setFecha(dia,mes,anio,hora,minuto,segundos); //ESTO PUEDE QUE NECESITE UN MANEJADOR
}

void modificarFecha(){
	string fecha;
	string hora;

	cout<<"Ingrese la fecha en el siguiente formato [dd/mm/aaaa hh:mm]"<<endl;
	do{
		fflush(stdin);
		cin >> fecha;
		if(fecha.length()!=10||fecha[2]!='/'||fecha[5]!='/'){
			cout<<"Formato incorrecto intente nuevamente"<<endl;
		}else{
			cin >> hora;
			if(hora.length()!=8||hora[2]!=':'||hora[5]!=':'){
				cout<<"Formato incorrecto intente nuevamente"<<endl;	
			}
		}
	}while(fecha.length()!=10&&hora.length()!=8);

	modificarFecha(fecha,hora);

}

void consultarFecha(){
	cout<<"La fecha es "<<reloj->getFecha()<<endl;
}

void deseaIngresarSala(bool &a){//consulta al usuario si desea ingresar mas salas
	char opcion;
	cout<<"Desea ingresar otra sala? [y/n] ";
	cin >> opcion;
	switch(opcion){
		case 'y':{
			break;
		}
		case 'n':{
			a = false;
			break;
		}
		default:{
		 cout<< "Opcion no valida." << endl;
		 deseaIngresarSala(a);
		}
	}
}

int switchConfirmarEliminacion(char opcion){
	int a;
	switch(opcion){
		case 'y':{
			a= 1;
			break;
		}
		case 'n':{
			a=0;
			break;
		}
		default:{
			cout<<"Opcion no valida."<<endl;
			a = switchConfirmarEliminacion(opcion);
		}
	}
	return a;
}

int switchConfirmar(char opcion){
	int a;
	switch(opcion){
		case 'y':{
			a= 1;
			break;
		}
		case 'n':{
			a=0;
			break;
		}
		default:{
			cout<<"Opcion no valida."<<endl;
			cin>>opcion;
			a = switchConfirmar(opcion);
		}
	}
	return a;
}

bool deseaIngresarCine(){
	char opcion;
	cout <<"Desea ingresar otro cine? [y/n] " <<endl;
	cin >> opcion;
	switch(opcion){
		case 'y':{
			// no hace nada
			return true;
			break;
		}
		case 'n':{
			return false;
			break;
		}
		default:{
			cout<<"Opcion no valida."<<endl;
			return deseaIngresarCine();
		}
	}
}

void ingresarCine(){
	bool ingresarCine = true;
	DtCine cine;
	Direccion direccion;
	string calle;
	int numero;
	while(ingresarCine){
		cout<<"Ingrese la calle donde se ubica el cine: ";
		cin >> calle;
		cout<<"Ingrese el numero de la direccion: ";
		cin >> numero;
		direccion.setCalle(calle);
		direccion.setNumero(numero);
		bool ingresarSala= true; 
		bool a = true;
		int capacidad = 0; 
		char opcion; 
		set<int> capacidadSalas; //lista de las capacidades de las salas que el usuario ingreso
		//DtSala [MAX_SALAS] salas;
		int cantSalas = 0;
		while(ingresarSala){
			cout<<"Ingrese la capacidad de la sala: ";
			cin >> capacidad;
			capacidadSalas.insert(capacidad);
			cantSalas++;
			deseaIngresarSala(ingresarSala);
		}
		cout<<"Confirmar alta del cine? [y/n] " <<endl;
		cin >> opcion;
		int b = 0;
		b = switchConfirmar(opcion);
		if (b==1){
			iconC->altaCine(direccion);
			iconC->agregarSalas(capacidadSalas);
		}
	ingresarCine = deseaIngresarCine();
	}
}

void confirmarEliminacion(string titulo){
	char opcion;
	int b=0;
	cout<<"Confirma eliminar la pelicula " << titulo << " ? [y/n] " << endl;
	cin >>opcion;
	b = switchConfirmarEliminacion(opcion);
	if(b==1){
		iconP->confirmar(); // no esto seguro si necesita pasarle el titulo  o no .
		//se elimina la pelicula,funciones y reservas asociadas a la misma.
	}
	//sino termina el caso de uso, vuelve al menu
}

void crearComentario(string titulo){
	string comentario;
	cout<<"Ingrese comentario: ";
	fflush(stdin);
	getline(cin,comentario);
	Sesion* sesion = Sesion::getInstancia();
	string nick = sesion->getNickName();
	set<DtComentario> respuestas;
	DtComentario comment = DtComentario(nick,comentario,respuestas);
	DtComentario* aux = &comment;
	cout<<"el comentario es"<<aux->getTexto()<<endl;
	iconP->comentarPelicula(aux,titulo);
}



int main(){
	system("color 0A");
	//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN | BACKGROUND_RED);
	fabrica = Fabrica::getInstancia();
	iconC = fabrica->getIControlCine();
	iconF = fabrica->getIControlFuncion();
	iconP = fabrica->getIControlPelicula();
	iconR = fabrica->getIControlReserva();
	iconU = fabrica->getIControlUsuario();
	iconS = fabrica->getIControlSala();
	Sesion* sesion = Sesion::getInstancia();
	reloj = Reloj::getInstancia();
	base = Base::getBase();
	string a = "18";string a1 = "09";string a3 = "1996"; string a4 ="16";string a5= "05";string a6= "00";
	reloj->setFecha(a,a1,a3,a4,a5,a6);//DD/MM/AAAA HH:MM:SS HORA POR DEFECTO
	cargarDatos();
	int opcion;
	while(opcion != 12){
		menu();
		cin>> opcion;
		
		switch(opcion){
			case 1: {//Iniciar Sesion
				system("clear");
				cout<<"\n_________INICIAR SESION___________\n"<<endl;
				string nick;
				cout << "Ingrese su nickname: ";
				cin >> nick;
				try{
					iconU->iniciarSesion(nick);
				}catch(invalid_argument &e){
					cout << e.what() << endl;
				}		
				break;//	OK	
			}
			////////////////////////////////////////////////////////////////////////////////////////////
			case 2:{ //Alta Cine		
				system("clear");
				cout<<"\n____________ALTA CINE_____________\n"<<endl;
				
				if (!sesion->getAdmin()){ //poner el !
					cout<<"Usted no es Administrador"<<endl;
				}else{
					ingresarCine();
				}
				break;
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
			case 3: {//Alta Funcion
				//system("clear");

				cout<<"\n____________ALTA FUNCION__________\n"<<endl;
				if (!sesion->getAdmin()){
					cout<<"Usted no es Administrador"<<endl;
				}else{
					char opcion;
					set<string> titulos = iconP->listarTituloPeliculas();//listar peliculas;
					for (auto aux : titulos){
						cout << aux << endl;
					}
						
					cout << endl;
					//liste las peliculas
					cout<<"Seleccione la pelicula"<<endl;
					string titulo;
					cin >> titulo;
					cout << endl;
					try {
						set<DtCine> cines = iconP->seleccionarPelicula(titulo);
						for (auto aux : cines){
							cout << aux << endl;
						}
						//listo los cines
						cout << endl;
						cout << "Seleccione un cine" << endl;
						int idCine;
						cin >> idCine;
						cout << endl;
						set<DtSala> salas = iconC->seleccionarCine(idCine);
						for (auto aux : salas){
							cout << aux << endl;
						} 					//HAY QUE ARREGLAR ESTO(FUNCION NO IMPLEMENTADA)
						//listo las salas
						cout << endl;
						cout << "Seleccione una sala" << endl;
						int idSala;
						cin >> idSala;
						iconS->seleccionarSala(idSala);
						iconF->ingresarHorario();
						iconF->altaFuncion(iconF->getHorario(), iconF->getFecha());
					} catch (invalid_argument e){
						cout << e.what() << endl;
					}
					//listar cines;
					/*sistema lista las salas del cine elejido inciando cantidad 
					de asientos que poseen y cuantos estan ocupados*/
					// se selecciona la sala y se indica fecha y hora de la funcion y se le da de alta
					//while el usuario quiera seguir ingresando funciones

				}
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////
			case 4: {//Crear Reserva
				system("clear");
				cout<<"\n___________CREAR RESERVA__________\n"<<endl;
				set<string> titulos = iconP->listarTituloPeliculas();
				for (auto i : titulos)
					cout << i << endl;
				cout << "\nSeleccione pelicula" << endl;
				string idP;
				cin >> idP;
				try {
					set<DtCine> cines = iconP->seleccionarPelicula(idP);
					for (auto aux : cines){
						cout << aux << endl;
					}

					cout << "Seleccione cine" << endl;
					int codC;
					cin >> codC;
					cout<<endl;
					set<DtFuncion> funciones = iconC->selectCine(codC);
					for (auto aux : funciones){
						cout << aux << endl;
					}
					cout << endl;
					cout << "Seleccione funcion" << endl;
					//selecciono la funcion
					int idF;
					cin >> idF;
					iconF->seleccionarFuncion(idF);
					int aux;
					cout << "Ingrese cantidad de asientos" << endl;
					cin >> aux;
					iconR->ingresarCantidadAsientos(aux);
					int a;
					cout << "Metodo de pago \n 1)Credito \n 2)Debito" << endl;
					bool salir = false;
					while (!salir){
						cin >> a;
						switch (a){
							case 1:{
								iconR->ingresarModoPago(1); //enumerado a credito
								cout << "Ingrese nombre de la financiera" << endl;
								string financiera;
								cin >> financiera;
								int descuento = iconR->ingresarFinanciera(financiera);
								cout << "Tiene un " << descuento << "% de descuento" << endl;
								salir = true;
								break;
								}
							case 2:{
								iconR->ingresarModoPago(0);//enumerado a debito
								cout << "Ingrese nombre del banco" << endl;
								string nomBanco;
								cin >> nomBanco;
								iconR->ingresarNomBanco(nomBanco);
								salir = true;
								break;
							}
							default:{
								cout << "Opcion invalida" << endl;
								break;
							}
						}

					}
					int monto = iconR->verPrecioTotal();
					cout << "Costo total: " << monto << endl;
					cout << "Desea confirmar [y/n]?" << endl;
					char b;
					cin >>b;
					salir = false;
					while (!salir){
						switch (b){
							case 'y':{
								iconR->setIdFuncion(idF);
								iconR->confirmar();
								salir = true;
								break;
							}
							case 'n':{
								cout << "Transaccion cancelada" << endl;
								salir = true;
								break;
							}
							default:{
								cout << "Opcion invalida" << endl;
								cin>>b;
								break;
							}
						}
					}
				} catch (invalid_argument e){
					cout << e.what() << endl;
				}
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////
			case 5:{ //Puntuar Pelicula
				system("clear");
				cout<<"\n_________PUNTUAR PELICULA_________\n"<<endl;
				set<string> titulos = iconP->listarTituloPeliculas();//listar peliculas;
				for (auto aux : titulos){
					cout << aux << endl;
				}
				cout << endl;
				cout<<"Seleccione una pelicula"<<endl;
				string titulo;
				cin >> titulo;
				try {
					iconP->recordarPelicula(titulo);
					cout << "Quiere ver el puntaje de " << titulo << " ? [y/n]" << endl;
					bool salir = false;
					while (!salir){
						char a;
						cin >> a;
						switch (a){
							case 'y':{
								iconP->listarPuntajes(titulo); 
								salir = true;
								break;
							}
							case 'n':{//no hago nada
								salir = true;
								break;
							}
							default:{
								cout << "Opcion invalida" << endl;
								break;
							}
						}
					}
					cout << "Quiere ingresar un puntaje ? [y/n]" << endl;
					salir = false;
					while (!salir){
						char a;
						cin >> a;
						switch (a) {
							case 'y':{
								cout << "Ingrese puntaje (de 1 a 5)" << endl;
								float puntaje;
								cin >> puntaje;
								iconP->ingresarPuntaje(puntaje);
								salir = true;
								break;
							}
							case 'n':{
								//no hago nada
								salir = true;
								//finalizar
								break;
							}
							default:{
								cout << "Opcion invalida" << endl;
								break;
							}
						}
					}
				} catch (invalid_argument e){
					cout << e.what() << endl;
				}
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////
			case 6:{ //Comentar Pelicula
				system("clear");
				cout<<"\n________COMENTAR PELICULA_________\n"<<endl;
				set<string> titulos = iconP->listarTituloPeliculas();//listar peliculas;
				for (auto aux : titulos){
					cout << aux << endl;
				}
				cout << endl;
				int id;
				cout<<"Seleccione una pelicula"<<endl;
				string titulo;
				cin >> titulo;
				try {
					iconP->recordarPelicula(titulo);
					bool salir = false;
					while (!salir){
						//mostramos comentarios
						cout << endl;
						cout << "Comentar/Responder o salir [c/r/s]" << endl;
						bool aux = false;
						while (!aux){
							char a;
							cin >> a;
							switch (a){
								case 'c':{
									crearComentario(titulo);
									aux = true;
									break;
								}
								case 'r':{
									iconU->mostrarComentarios(titulo);
									cout<<"Seleccione el comentario a cual responder"<<endl;
									cin >> id;
									
									cout<<"Ingrese Comentario: "<<endl;
									string comentario;
									fflush(stdin);
									getline(cin,comentario);
									iconU->responderComentario(id,comentario,titulo);

									//crear un nuevo comentario y meterlo en en set de comentarios que entro (cometnarios tiene que tener un map tambien y no set)

									//en el medio va algo
									aux = true;
									break;
								}
								case 's':{
									aux = true;
									salir = true;
									break;
								}
								default:{
									cout << "Opcion invalida" << endl;
									break;
								}
							}
						}
					}
				} catch (invalid_argument e){
					cout << e.what() << endl;
				}
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////
			case 7:{ //EliminarPelicula
				system("clear");
				cout<<"\n________ELIMINAR PELICULA_________\n"<<endl;
				if (!sesion->getAdmin()){
					cout<<"Usted no es Administrador"<<endl;
				}else{
					try {
						set<string> titulos = iconP->listarTituloPeliculas();//listar peliculas;
						for (auto aux : titulos){
							cout << aux << endl;
						}
						cout << endl;
						string titulo;
						cout<<"Seleccione la pelicula que desea eliminar: "<<endl;
						cin >> titulo;
						iconP->recordarPelicula(titulo);
						confirmarEliminacion(titulo); //no lleva nada | albert
					} catch (invalid_argument e){
						cout << e.what() << endl;
					}
				}
				break;
			}	
			//////////////////////////////////////////////////////////////////////////////////////////////
			case 8:{ //Ver Informacion de Pelicula
				system("clear");
				cout<<"\n______VER INFORMACION PELICULA____\n"<<endl;
				set<string> titulos = iconP->listarTituloPeliculas();//listar peliculas;
				for (auto aux : titulos){
					cout << aux << endl;
				}
				cout << endl;
				bool salir = false;
				try {
					while (!salir){
						cout<<"Seleccione una pelicula"<<endl;
						cout << endl;
						string titulo;
						cin >> titulo;
						DtInfoPelicula dato = iconP->listarInfoPelicula(titulo);
						cout << dato << endl; 
						bool aux = false;
						while (!aux){
							cout << "Desea salir ? [y/n]" << endl;
							char a;
							cin >> a;
							switch (a){
								case 'y':{
									//no hago nada
									aux = true;
									salir = true;
									break;
								}
								case 'n':{
									set<DtCine> cines = iconP->seleccionarPelicula(titulo);
									for (auto aux : cines){
										cout << aux << endl;
									}
									cout << endl;
									cout << "Seleccione cine para listar informacion extra" << endl;
									int codC;
									cin >> codC;
									cout << "Desea listar funciones ? [y/n]" << endl;
									bool aux1 = false;
									while (!aux1){
										cin >> a;
										switch (a) {
											case 'y':{
												set<DtFuncion> funciones = iconC->selectCine(/*idP no esta declarada,*/ codC);
												for (auto aux : funciones){
													cout << aux << endl;
												}
												cout << endl;
												aux1 = true;
												break;
											}
											case 'n':{
												//no hago nada;
												aux1 = true;
												break;
											}
											default:{
												cout << "Opcion invalida" << endl;
												break;
											}
										}
									}
									break;
								}
								default:{
									cout << "Opcion invalida" << endl;
									break;
								}
							}	
						}
					}
				} catch (invalid_argument e){
					cout << e.what() << endl;
				}
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////
			case 9:{ //Ver Comentarios y Puntajes de Pelicula
				system("clear");
				cout<<"\n_VER COMENTARIOS Y PUNTAJES DE PELICULA_\n"<<endl;
				//listar titulo y poster de las peliculas registradas
				set<string> titulos = iconP->listarTituloPeliculas();//listar peliculas;
				for (auto aux : titulos){
					cout << aux << endl;
				}
				cout << endl;
				cout<<"Seleccione una pelicula"<<endl;
				string titulo;
				cin >> titulo;
				cout<<"\nPuntaje promedio: ";
				iconP->mostrarPuntajePromedio(titulo);
				cout<<"\nComentarios: ";
				iconU->mostrarComentarios(titulo);
				cout<<"\nPuntajes:\n\n";
				iconP->listarPuntajes(titulo);
				cout << endl;
				break;
			}
			//////////////////////////////////////////////////////////////////////////////////////////////
			case 12: {//salir
				system("exit");
				cout<< "Saliendo..."<< endl;
				break;
			}
			///////////////////////////////////////////////////////////////////////////////////////////////
			case 11: {//MODIFICAR FECHA
				system("clear");
				modificarFecha();
				break;
			}	
			///////////////////////////////////////////////////////////////////////////////////////////////
			case 10: {//CONSULTAR FECHA
				consultarFecha();
				break;
			
			}
			//////////////////////////////////////////////////////////////////////////////////////////////
			default:{
				cout<< "Error, opcion invalida."<< endl;
			}
		}
	}
}