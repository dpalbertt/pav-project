#ifndef BASE_H
#define BASE_H
#include <iostream>
#include <string.h>
#include <set>
#include "Cine.h"
#include "Comentario.h"
#include "ManejadorCine.h"
#include "ManejadorUsuario.h"
#include "ManejadorPelicula.h"
#include "ManejadorPuntaje.h"
#include "ManejadorFuncion.h"
#include "Credito.h"
#include "Debito.h"
#include "Direccion.h"
#include "DtCine.h"
#include "DtComentario.h"
#include "DtFuncion.h"
#include "DtInfoPelicula.h"
#include "DtPelicula.h"
#include "DtPeliculaCompleta.h"
#include "DtPuntaje.h"
#include "DtSala.h"
#include "Fecha.h"
#include "Funcion.h"
#include "Horario.h"
#include "Pelicula.h"
#include "Puntaje.h"
#include "Reloj.h"
#include "Reserva.h"
#include "Sala.h"
#include "Sesion.h"
using namespace std;



class Base {
private: 
	Base();
	static Base* instancia;
public:
	~Base();
	static Base* getBase();
	void crearDatos();
};

#endif